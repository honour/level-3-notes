\section*{Web Services and REST}

`Webscraping' is still popular, but inherently unreliable.
There is a need to access web services programmatically.

Distributed systems technologies must support industry requirements, which have moved rapidly to the adoption of encoding formats for describing and exchanging data.

A \textbf{web service} is a piece of business logic located on the internet, accessed over standard protocols like HTTP and SMTP\@.
Web services do not replace other distributed system technologies.
They allow us to build larger-scale, loosely-coupled DS without constraining technology used to develop components.

\textbf{XML} is often used for structuring and exchanging data because it is easily extended, allows data to self-describe and is text-based which allows portability and human-readability.
\textbf{JSON} is becoming more popular, because it's also self-describing, language independent and easy to understand, but is also incredibly lightweight.

Web services support the development of highly distributed SOAs, where each WS is a self-contained, self-describing modular unit as part of a larger system.
There are three roles for participants in an SOA: \textbf{Provider} who publishes services via a broker; \textbf{Requestor} who uses the broker to find, and bind to, services; \textbf{Broker} acting as a `Yellow Pages' directory for services.

Web services are platform and implementation independent.
A client cannot tell what languages, OS or platforms are in use.
They can be described using a Service Description Language, published to a registry of services, discovered through a standard mechanism, invoked through a declared API (usually over a network), and composed with other services.

Web services standardise the packaging of information for transport, using protocols like HTTP, SMTP and FTP using encodings like JSON \& XML\@.
They support different modes of communication, including synchronous `RPC-style' and asynchronous (document transfer) methods.
They also provide an interoperability abstraction of conventional RPC/RMI\@.

\subsection*{REST}

The uniform interface has a constrained set of well-defined operations, and a constrained set of content-types.
REST uses a protocol that is client-server, stateless, cacheable and layered.
The REST methodology is identical to how a browser requests a web page or sends data to a web server.

In practice, web pages are resources, accessible using a universal syntax -- URIs.
The operations in REST are simply HTTP verbs.
REST is not a standard, but rather an architectural style which prescribes the use of standards, like HTTP, URI, XML, etc.

\vspace*{-0.5cm}

\begin{figure}[ht!]
    \centering
    \begin{tikzpicture}
        \coordinate (A) at (0,0);
        \coordinate (B) at (2.5,0);
        \coordinate (C) at (1.25,{sqrt(4.5)});
        \coordinate (centroid) at (barycentric cs:A=1,B=1,C=1);
        \draw (A) -- (B) -- (C) -- cycle;

        \node[align=center] at (centroid) {\textbf{REST}};
        \node[left, align=center, text width=3cm] at (A) {Verbs \\ \textcolor{Red}{(constrained)} \\ \textcolor{RoyalBlue}{e.g.\ GET}};
        \node[right, align=center, text width=3cm] at (B) {Content Types \\ \textcolor{Red}{(constrained)} \\ \textcolor{RoyalBlue}{e.g.\ HTML}};
        \node[above, align=center, text width=3cm] at (C) {Nouns \\ \textcolor{Green}{(unconstrained)} \\ \textcolor{RoyalBlue}{e.g.\ https://a.com}};
    \end{tikzpicture}
    \label{fig:rest-triangle}
    \caption*{The REST Triangle}
\end{figure}

A client references a web resource using a URL\@.
A \textbf{representation} of the resource is returned.
The representation places the client into a certain \textbf{state}.
By exploring hyperlinks in the returned resource the client moves into different \textbf{states}.
With each resource representation the client is therefore \textbf{transferring} states.

Benefits of REST include that it provides improved response times by supporting caching, it improves scalability by reducing the need to maintain communication state, it depends less on vendor software and does not require a separate resource discovery mechanism.
