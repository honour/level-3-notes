\section*{Architectures}

Organisation of distributed systems is largely about the software components that make up a system.
\textbf{Software architectures} state how the various components should be organised.
The goal is to separate applications from underlying platforms using the middleware layer.
The instantiation of software architectures, i.e.\ placing the components onto real machines is called \textbf{system architecture}.

Architectural style is formulated in terms of components with well-defined interfaces, the way these components are connected, the data they exchange and how this all comes together to form a system.

\subsection*{Layered Architecture}

\includegraphics[width=\linewidth]{assets/layered-architecture}

In a layered architecture, components make calls to the ones below, expecting a response which can be passed upwards.
An example of a layered architecture is the \textbf{PAD Model}.

Many client-server applications are formed from 3 layers: the \textbf{Presentation} layer, containing components required to interface with users, the \textbf{Application} layer, containing core functionality of the system, and the \textbf{Data} layer, responsible for persistent storage of the data on which the application layer operates.

\textbf{Load balancers} receive client requests, direct the workload within the datacenter, and return the results to the client, whilst hiding these internals from the user.

\subsection*{Object-based Architecture}

In this architecture type, components are objects, connected to each other with procedure calls.
Objects may be placed on different machines and communicate via a network, for example using \textbf{Remote Method Invocation (RMI)}.
There are method stubs on the client and server, referred to as a \textbf{proxy} and \textbf{skeleton} respectively.

\subsection*{Resource-based Architecture}

Here, the system is viewed as a collection of resources, individually managed by components.
Resources may be added, removed, retrieved and modified by (remote) applications.

Resources are identified by a single naming scheme, with all services offering the same interface.
Messages sent to and from a service are fully self-described, and after executing an operation the system has no persistent information about the caller.

This is the approach taken in \textbf{Representational State Transfer (REST)} web services, using the \textbf{CRUD} operations.

\subsubsection*{Example: AWS S3}

Objects are placed into buckets.
An object $o$ in bucket $b$ would be referred to using the following URI:

\texttt{https://b.s3.amazonaws.com/o}

All operations are performed through HTTP, creating buckets/objects is done using POST requests, objects are listed with a GET request to the bucket name, and a single object can be fetched with a GET request to the full object URI.

\subsection*{Event-based Architecture}

This architecture type has a strong separation between processing and coordination.
The distributed system is a collection of autonomously operating processes, with coordination encompassing the communication and cooperation between processes.

There are two types of coordination -- \textbf{mailbox coordination}, where messages are placed into a shared mailbox and picked up by processes, and \textbf{event-based coordination}, where processes will publish notifications describing an event, which other processes can subscribe to and handle.

\subsection*{System Architectures}

System architecture is the instantiation of a software architecture, including the software components, their interaction, and placement on real machines.

\subsubsection*{Centralised Organisation}

This is the typical client-server model.
Some processes are offering services, some processes are consuming services.
They can be placed on different machines, and clients follow the request/reply model for consumption.

2-tier architecture refers to the split of tasks between the client and service machines.
This can be done in a number of ways.
3-tier architecture is an improvement to this.
Instead, there is the user interface (or presentation) layer, and then an application and data tier (similar to the PAD model).

\subsubsection*{Decentralised Organisation}

This is the concept of a peer-to-peer model.
Every process is considered equal, they all act as a client and server at the same time.

In a \textbf{structured P2P} system, nodes are organised in a overlay which follows a specific, deterministic topology used to lookup data.
A semantic-free index is used, each data item is uniquely associated with a key.
It's common for the key to be a hash of the data item being stored.

\subsubsection*{Hybrid Organisation}

These are a specific class of distributed system in which client-server solutions are combined with decentralised ones.
An example of this is edge-server systems.
These are deployed on the internet and placed at the edge of the network -- the boundary between enterprise networks and the actual internet as provided by ISPs.
These edge servers can then be used to optimise content and application distribution.
