\section*{Fault Tolerance}

A component provides services to clients.
To provide services, the component may rely on services from other components.
That is, the component may depend on others.

A component $C$ depends on a component $C^{*}$ if the correctness of $C$'s behaviour depends on the correctness of $C^{*}$'s behaviour.

Requirements related to dependability:

\begin{tabular}{|l|p{0.66\linewidth}|}
    \hline
    \textbf{Requirement} & \textbf{Description} \\
    \hline
    Availability & Readiness for usage \\
    \hline
    Reliability & Continuity of service delivery \\
    \hline
    Safety & Very low probability of catastrophes \\
    \hline
    Maintainability & How easy can a failed system be repaired \\
    \hline
\end{tabular}

The reliability $R(t)$ of a component $C$ is the conditional probability that C has been functioning correctly in the interval $[0,t)$ given $C$ was functioning correctly at $t=0$.

The availability $A(t)$ of a component $C$ is the average fraction of time that $C$ has been up and running in the interval $[0,t)$.
Long term availability is $A(\infty)$.

Mean time to Failure ($MTTF$): The average time until a component fails.
Mean time to Repair ($MTTR$): The average time needed to repair a component.
Mean time between failures ($MTBF$): $MTTF + MTTR$.

\[
    A = \frac{MTTF}{MTBF} = \frac{MTTF}{MTTF+MTTR}
\]

Reliability and availability only make sense if we have an accurate notion of what failure actually is.

A failure is when a component is not living up to specifications (like a crashed program).
An error is part of a component that can lead to failure (like a programming bug).
A fault is the cause of an error (like sloppy programming).

Fault prevention is preventing the occurrence of faults, for example through software verification and validation.
Fault tolerance is building components in such a way that they can mask the occurrence of faults, for example by building each component by two independent programmers.
Fault removal is reducing the presence, number, or seriousness of faults, through software verification and validation.
Fault forecasting is estimating the current presence, future incidence and consequences of faults, for example by examining code quality.

There are various different types of failure.
Crash failures occur when a program halts, but was working correctly until then.
Omission failures are when a server fails to respond to incoming requests.
Receive omissions are when it fails to receive incoming messages, send omissions are when it fails to send messages.
Timing failures occur when a program responds outside a specified timing interval.
Response failure occurs when the response provided is incorrect.
This includes value failure -- the value of the response is wrong, and state-transition failure -- the program deviates from the correct flow of control.
Finally arbitrary (or Byzantine) failures occur when a program may produce arbitrary responses at arbitrary times.

Arbitrary failures are sometimes classified as malicious.
Omission failures are when a component fails to take action that it should have taken, commission failures are when a component takes action that it should not have.

Deliberate failures, whether omission or commission, are usually a security problem.
In general, distinguishing between deliberate and unintentional failure is very hard.

There are different types of redundancy.
Information redundancy is adding extra bits to data units so that errors can be recovered when bits are garbled (e.g.\ Hamming codes).
Time redundancy is designing a system such that an action can be performed again if anything went wrong (e.g.\ retransmission request to a server when lacking an expected response).
Physical redundancy is adding equipment or processes in order to allow one or more processes to fail.
This is used extensively in distributed systems.

We can protect against malfunctioning processes through process replication, and organising multiple processes into process groups.
There are flat and hierarchical groups.

\includegraphics[width=\linewidth]{assets/process-groups}

A $k$-fault tolerant group is when a group can mask and $k$ concurrent member failures.
$k$ is called the degree of fault tolerance.

With halting failures (crash/omission/timing) we need a total of $k+1$ nodes, as no member will produce an incorrect result, so the result of one member is enough.
With arbitrary failures, we need $2k+1$ members so that the correct result can be decided by majority vote.

It is a prerequisite that in a fault-tolerant process group, each non-faulty process executes the same commands, in the same order as every other non-faulty process.
Non-faulty group members also need to reach consensus on which command to execute next.

\subsection*{Consensus in Faulty Systems with Crash Failures}

This can be done by flooding-based consensus.
The system model is as follows:

A process group $P = \{P_1 \ldots P_n\}$; Fail-stop failure semantics, that is, having reliable failure detection; A client contacts a process $P_i$ requesting it to execute a command; Every $P_i$ maintains a list of proposed commands, received directly from clients or fellow group members.

In round $r$, process $P_i$ multicasts its known set of commands $C_i^r$ to all other processes.
At the end of $r$, each $P_i$ merges all received commands into a new $C_i^{r+1}$.
The next command, $cmd_i$, is selected through a globally shared, deterministic function: $cmd_i \leftarrow select(C_i^{r+1})$.

\includegraphics[width=\linewidth]{assets/flooding-based-consensus}

$P_2$ received proposed commands from all other processes, so it can make a decision.
$P_3$ may have detected that $P_1$ crashed, but doesn't know whether $P_2$ received anything from it, so it cannot be sure it has the same information as $P_2$.
The same is the case for $P_4$.
For this reason, $P_3$ and $P_4$ postpone their decision until next round.
$P_2$ makes a decision for the round and broadcasts that the all others.
$P_3$ and $P_4$ are able to make a decision at round $r+1$, and choose to execute the same command as $P_2$.

\subsection*{The Two Generals Problem}

The concept here, is that two armies are attacking a city, and want army 1 to attack if and only if army 2 does.

The problem is however, if army 1 sent a date to army 2, which they received, but their response got lost in transit, this is indistinguishable from the message from army 1 never being received.

One possibility is that army 1 always attacks, even if they don't get confirmation.
Army 1 could send multiple messengers, to increase the possibility that one gets through.
However, if all get captured, army 2 doesn't know about the attack, causing a loss.

Another solution is that army 1 only attacks if they get confirmation back from army 2.
This allows army 1 to be safe, but army 2 now know that army 1 will only attack if their message gets through.
This puts army 2 in the exact same situation that army 1 is in in the first scenario.

\textbf{There is no common knowledge, the only way of sharing knowledge is to communicate it}.

\includegraphics[width=\linewidth]{assets/two-generals-problem}

Above is an application of the two generals problem in the real world.
It is a challenge to design platforms which handle this effectively.

\subsection*{Byzantine Generals Problem}

The situation is that many divisions of an army are set up around a city, each division with its own general.
These generals can only communicate by passing messages.
After observing the enemy they must come up with a plan.

The generals can only decide to attack or retreat.
Some generals may have different preferences, but the important thing is that the generals come to a common decision, because a partial attack would be worse than a coordinated attack or retreat.

However, it is impossible to know which (if any) generals are traitors, trying to prevent the loyal ones from reaching agreement.
The generals must have an algorithm to guarantee all loyal generals decide on the same plan, and that a small number of traitors can't cause the wrong decision to be made.

\includegraphics[width=\linewidth]{assets/byzantine-generals}

For example, if army 1 tells armies 2 and 3 to attack, but army 2 passes a message to army 3 informing them that army 1 said to retreat, it is impossible to know which of the following situations actually occurred:

\includegraphics[width=\linewidth]{assets/byzantine-generals-1}
\includegraphics[width=\linewidth]{assets/byzantine-generals-2}

Up to $f$ generals may be malicious.
Honest generals do not know who the traitors are.
The traitors may collude, but the honest generals must decide on a plan.

The theorem is that $3f+1$ generals are required to tolerate $f$ malicious generals (so $< \frac{1}{3}$ may be malicious).
Digital signatures help, but the problem is still difficult to solve.

\subsection*{Consensus in Faulty Systems with Arbitrary Failures}

We consider process groups in which communication between processes is inconsistent.
Below, nodes in gray represent a faulty process.

\begin{minipage}{0.5\linewidth}
    \centering
    \begin{tikzpicture}[thick, node distance=1cm]
        \node[circle, draw, minimum size=0.5cm] (P1) at (90:1.5) {P1};
        \node[circle, fill=gray!50, draw, minimum size=0.5cm] (P2) at (330:1.5) {P2};
        \node[circle, draw, minimum size=0.5cm] (P3) at (210:1.5) {P3};
        \draw[->] (P1) -- node[above left] {a} (P3);
        \draw[->] (P1) -- node[above right] {a} (P2);
        \draw[->] (P2) -- node[above] {b} (P3);
    \end{tikzpicture}
\end{minipage}%
\begin{minipage}{0.5\linewidth}
    \centering
    \begin{tikzpicture}[thick, node distance=1cm]
        \node[circle, fill=gray!50, draw, minimum size=0.5cm] (P1) at (90:1.5) {P1};
        \node[circle, draw, minimum size=0.5cm] (P2) at (330:1.5) {P2};
        \node[circle, draw, minimum size=0.5cm] (P3) at (210:1.5) {P3};
        \draw[->] (P1) -- node[above left] {a} (P3);
        \draw[->] (P1) -- node[above right] {b} (P2);
        \draw[->] (P2) -- node[above] {b} (P3);
    \end{tikzpicture}
\end{minipage}

The system on the left shows the improper forwarding of messages, whilst the one on the right showing different processes being given differing information.

The system is modelled as a process group, consisting of $n$ members, of which one is the primary, $P$, and the remaining $n-1$ are backups, $B_1 \ldots B_{n-1}$.
A client sends a value $v \in \{ T, F\}$ to $P$.
Messages may be lost, but this can be detected; however messages cannot be corrupted beyond detection.
A receiver of a message can reliably detect the sender.

The Byzantine agreement has the following requirements: \\
\textbf{BA1}: Every non-faulty backup stores the same value. \\
\textbf{BA2}: If $P$ is non-faulty then every non-faulty backup stores what $P$ has sent.

Some observations here are that if the primary is faulty, BA1 says that the backups may store the same value, that is different (and therefore wrong) from what the client sent.
If the primary is not faulty, satisfying BA2 implies BA1 is satisfied.

The situation below shows attempting to tolerate failure of one process, i.e\ $k=1$, when there are only $3k$ processes:

\includegraphics[width=\linewidth]{assets/3k-processes}

On the left, B1 and B2 forward their values to each other to check, but they each received the opposite value so are left with $\{T,F\}$.
No consensus can be reached.

On the right, B1 sends the incorrect value to B2, so it now has $\{T,F\}$.
No consensus can be reached.

The situation below shows attempting to tolerate failure of one process, i.e\ $k=1$, when there are $3k+1$ processes instead:

\includegraphics[width=\linewidth]{assets/3k+1-processes}

On the left, after 2 rounds of communication, the backups will all have the values each other were sent, resulting in $\{T,T,F\}$.
The consensus can be reached that $P$ sent out $T$.

On the right, B1 and B3 will come to the same conclusion, even if B2 has informed them that $P$ sent out $F$.
The consensus can be reached that $P$ sent out $T$.

\subsection*{System Models}

The two generals problem is a model of networks, whilst the Byzantine generals problem is a model of node behaviour.
In real systems, both of these may be faulty.
We need to capture assumptions in our system model about network behaviour, node behaviour and timing behaviour.
