\section*{Web Crawling}

A search engine maintains the processes of Web Crawling, Indexing, and Searching in near real-time.
To build a search engine, we first need a copy of the pages we want to search.
Search engines fetch this information by crawling from one web page to another.
The crawler follows the hyperlinks in each page to discover new ones.
Finding and downloading webpages automatically is called crawling/spidering, and a program that downloads the pages is called a web crawler.

\subsection*{Challenges}

The sheer scale of the web makes crawling a difficult task.
Pages are constantly being created and removed, and these pages are usually not under the control of people building the search engine.
Website owners don't like their sites being crawled, and some data to copy might only be accessible via forms, which is very difficult to automate.

\subsection*{How They Work}

Crawlers have two jobs -- downloading pages and finding URLs.
The crawler starts with a set of seeds, which is the set of URLs given as starting parameters.
These seeds are added to a URL queue.
Once a page is downloaded, it's parsed to find link tags to other useful URLs.
If the crawler finds a new URL that it's not seen before, it's added to the queue (known as the \textbf{frontier}).
The frontier might be a standard queue, or it could be ordered to move important pages to the front.
This process continues until the crawler runs out of disk space, or runs out of links in the queue.

Fetching hundreds of pages at once is good for the crawler, but not good for web servers on the other end.
If the server isn't very powerful, it could spend all its time handling requests from crawlers instead of real users.
This behaviour can make system admins angry, and can result in IP address blocks.

\subsection*{Politeness Policies}

To avoid overload, web crawlers use politeness policies.
Reasonable crawlers don't fetch more than one page at a time from a particular server.
They also wait a few seconds and sometimes minutes between requests to the same server.
To support this, the queue is split into a single queue per web server.
The crawler is free to read page requests only from queues which haven't been accessed in the politeness window.

For example, if a crawler can fetch 100 pages per second, and its policy means it can't fetch a page more often than 30 seconds from each server.
This means it'll need at least 3000 different web servers in the queue to achieve its maximum throughput.
Since many URLs will come from the same servers, the queue will require tens of thousands of URLs before reaching peak throughput.

\subsection*{robots.txt}

Even crawling slowly can anger some web admins who object to the process entirely.
They can create a robots.txt file on their server.
This file is split into blocks for each user agent, and can contain allow and disallow rules for various resources.
An optional (non-standard) crawl delay can also be added.

\subsection*{Detecting Robots}

Very quick requests to the same page is the most obvious feature of a badly designed bot.
For this reason, robots should respect the fair use policy and allow sufficient time between requests.

Trap Links in some pages can also catch bots.
They'll be invisible to humans, but naive robots will follow these links, enabling further action against them.
Robots that follow the robots.txt file will likely avoid these trap links.

Fictitious Resources can be dynamically created links that will send some robots into infinite recursion, destabilising them.
Crawlers can avoid this by having a maximum depth that they will follow.

The User-Agent HTTP header usually contains information about what is making the request.
Unwanted bots that identify themselves with this header can be actively blocked.
Crawlers can avoid this by User-Agent spoofing.

Crawlers that make requests at regular rates can also be easily detected.
Sophisticated robots will randomise this timing.

Detecting blocks can be done by looking for CAPTCHA pages, unusual content delivery delay, and the frequent appearance of redirect and block status codes.

\subsection*{Maintaining Page Freshness}

Webpages are constantly added, deleted, and modified.
To keep an accurate view of the web, a crawler must constantly revisit pages to see if they've changed to maintain freshness.
The HTTP HEAD request method, which only returns header information is useful for checking page changes.
The Last-Modifier header indicates the time the page content was change.
This is compared with the date from a previous GET request.

A HEAD request reduces the cost of checking, but doesn't eliminate it.
It's just not possible to recheck every page each minute.
It does little good to check sites that are rarely updated.
Hence, a crawler can be made to learn about the frequency of changes, to guess when it should check next.

\subsection*{Sitemaps}

A sitemap is an XML file stored in the site's root directory
It contains a list of URLs and data about them, such as modification time and modification frequency.
A priority value can also be included, of which the default is 0.5.
This tells crawlers which pages are more important.
A sitemap tells search engines about pages it might not otherwise find.

\subsection*{Distributed Crawling}

For crawling single websites, one computer is sufficient.
However, crawling the whole Web requires many computers that are devoted to crawling.
This helps put the crawler in closer proximity to the sites, reduces the number of sites a particular crawler has to store, and reduces the required computing resources for each machine.

\subsection*{The Conversion Problem}

Search engines are built to search through text.
Text is stored on computers in hundreds of different, incompatible formats.
It's not uncommon for commercial search engines to support over one hundred file types.
The most common way to handle new file formats is to use a conversion tool which outputs a tagged format like HTML or XML\@.
Documents could be converted to plaintext, but this can strip the file of useful information.
Things like headings and bold text should be given preferential treatment during indexing for example, so we don't want to lose this.
