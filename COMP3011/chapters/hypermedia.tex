\section*{Hypermedia}

There are hundreds of media types that are available for use on the web.
But we need to choose the media type that's best for a particular application.

\subsection*{Plaintext}

This is the simplest and most obvious format.
It doesn't have any structure or formatting, except what is dictated by the rules of natural language.
It can be used for very short API responses targeted at humans.
MIME type: \texttt{text/plain}.

\subsection*{HTML (Hypertext Markup Language)}

This is the oldest and most widely used hypermedia format.
It dates back to the early days of the web, and is a textual data format that can be read and understood by humans and machines.
It's ideal for serving human-readable documents, and is the most widely used format for this purpose.
It uses tags to denote the structure of a document, such as headings and paragraphs.
Browsers don't display these tags, but parses them to understand the content.

Early versions of HTML only included tags to describe structure and textual content.
Newer versions are augmented with features for adding images, video, audio, and scripting languages like JavaScript.
HTML has also been augmented by CSS to declare how documents should be rendered.
Without CSS, it's up to the client to decide how to render a document.
MIME type: \texttt{text/html}.

\textbf{Advantages:} HTML is widely used, with lots of support, tools, and libraries for processing on various platforms.
It lends itself to applications that require serving human-readable data.
The latest versions are quite advanced and support many features to deliver content-rich web pages.
It can also embed other formats like XML\@.

\textbf{Disadvantages:} Because it's designed to describe human-readable documents, HTML isn't the best choice for representing pure data intended for machine-machine communication.
Useful content is surrounded by lots of metadata, increasing bandwidth.

\subsection*{XML (Extensible Markup Language)}

XML was first published in 1998.
It's a textual markup language like HTML, and also uses tags.
The main purpose is data storage and serialisation.

Unlike HTML, which represents the structure of human-readable documents, XML is used to represent pure data, regardless of how it'll be rendered.
While HTML tags are predefined and have specific meanings, XML tags can be anything.
The XML standard only has restrictions on the syntax of the document, not on the semantics of tags.
Because of this, anyone can invent their own XML-based data format, to add semantics to the tags.
When web protocols like SOAP were developed, it was natural to choose XML\@.
MIME type: \texttt{application/xml} (or \texttt{text/xml}).

\textbf{Advantages:} XML can be the underlying syntax for any media type.
Because it's text based, it's readable and understandable by humans and machines.
It lends itself very nicely to tree-like structures.
It's platform independent.
You can invent your own XML-based formats.

\textbf{Disadvantages:} The files contain lots of metadata, like tag delimiters, and each tag usually requires seven lexical elements.
This makes it heavy compared to formats like JSON\@.
THe standard doesn't have any semantics, so they need to be defined in a separate XML schema file.

\subsubsection*{RSS (Really Simple Syndication)}

There are hundreds of formats that use XML as the underlying representation.
RSS allows client applications to access updates to websites.
MIME type: \texttt{application/rss+xml}.

\subsubsection*{Atom}

Atom is an alternative to RSS\@.
MIME type: \texttt{application/atom+xml}.

\subsubsection*{SOAP}

The Simple Object Access Protocol.
It's used in SOAP-based web services.
MIME type: \texttt{application/soap+xml}

\subsection*{JSON (JavaScript Object Notation)}

JSON is a human-readable, language independent format.
It was devised in around 2001, and is built on two structures: a collection of key-value pairs, and ordered lists of values.
These are universal data structures supported by all programming languages.
It makes sense for a data format that is language independent to be based on these structures.
Like XML, it's a syntax specification, semantics are not defined.
MIME type: \texttt{application/json}.

\textbf{Advantages:} It provides a lower overhead alternative to XML, which reduces storage and bandwidth.
It's easy to parse, and be used to serialise almost all kinds of data.

\textbf{Disadvantages:} In addition to the limitations of other text-based media types, JSON doesn't support comments.

\subsection*{Collections}

Collections are designed not to represent one specific problem domain, but to fit a pattern -- the collection -- that shows up over and over again, in various domains.
They all look the same, and have the same protocol semantics.
A collection is a special type of resource.
A resource is anything important enough to have been given its own URL\@.
A collection resource exists mainly to group other resources together.
Its representation focuses on links to other resources, and it may include snippets from the representations of those other resources.

\subsubsection*{Collection+JSON}

The Collection+JSON standard defined a representation format based on JSON\@.
It also defines the protocol semantics for the HTTP resources that serve that format.
It's an object with five predefined slots for application specific data:

\begin{itemize}
    \itemsep-0.75em
    \item href -- A link to the collection itself
    \item items -- Links to the members of the collection, and partial representations of them
    \item links -- Links to other resources related to the collection
    \item queries -- Hypermedia controls for searching the collection
    \item template -- A hypermedia control for adding a new item to the collection
\end{itemize}

\subsubsection*{How a Collection Works}

There's very little more to Collection+JSON than the information above, because it was defined without any application semantics.
A collection responds to GET by serving a representation.
Although collection standards don't say much about what an item should look like within a collection, they go into detail about what the collection's representation should be.
The media type of the representation tells you what you can do with the resource.

The defining characteristic of a collection is its behaviour under POST\@.
Unless a collection is read-only, a client can create a new item inside the collection with a POST request.
When you POST a representation to a collection, the server will create a new resource, and it'll become the latest member of the collection.

None of the three big collection standards define how a collection should respond to DELETE\@.
Some applications implement DELETE by deleting the collection, others delete the collection and all the resources in it.
THe main collection standards all define an item's DELETE response, but that is just restating the HTTP standard.
