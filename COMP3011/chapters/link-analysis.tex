\section*{Link Analysis}

The Web has billions of pages, many of which will have little/no useful content.
For example, some may be personal blogs, or picture albums.
In many cases, only a few of the pages containing a search term are popular and useful to many.
A search engine needs to choose the most popular pages.
We can use the links connecting pages as a way to rank them.

The text of an anchor (\texttt{<a>}) tag is called the `anchor text', and is usually two or three words that concisely describe a topic.
It's been found that many queries are similar to anchor text in that they're short descriptions of pages.

Anchor text analysis searches through all links in the collection of pages, looking for anchor text that matches the user query.
For each match, we add 1 to the score of the destination page.
This algorithm has some faults, for example for the query ``Click Here'', and it doesn't take into account the rank of the source page.
A more useful way of using anchor text is as an extra field for a page.

The simplest form of page ranking is based on counting the number of links pointing to a certain page.
The fact that a link exists at all is a vote of importance for a page.
The higher the count, the higher the page is ranked.
This is based on the assumption that important pages are linked to by many others.
The problem with this method is that it doesn't take into account the source page importance.

\subsection*{PageRank}

One of the most popular page ranking algorithms was created/used by Google, and is called \textbf{PageRank}.
It's based on the idea of a random web surfer.
We imagine that a user is aimlessly clicking through pages.
Each time a page loads, they choose to click one of the links on a page.
If a link is clicked, it's not based on any preference, but is just a random click.
We assume the user is bored enough that they do this forever.

Even though the page through the pages is random, more popular pages will appear more because they're linked to more often.
So we expect that the surfer would for example look at a university website more often than a personal blog, but less often than a news site.

PageRank provides a probability value, for example if you looked at the surfer's screen, the probability that they are on a certain website is that page's PageRank value.
Every page on the internet has a PageRank, and is uniquely determined by the link structure of web pages.

PageRank has the ability to distinguish popular pages, those with many incoming links, or those that have links from popular pages, from unpopular ones.
The PageRank value can help search engines sift through millions of pages that contain a search term, to find the best option.

\subsubsection*{Computing PageRank}

If we suppose that the web consists of three pages, A, B, and C, that are linked as shown in the diagram, the PageRank of C will depend on the PageRanks of A and B\@.

\begin{figure}[ht!]
    \centering
    \begin{tikzpicture}[thick, node distance={20mm}]
        \node[circle, draw] (a) {A};
        \node[circle, draw] (b) [below left of=a] {B};
        \node[circle, draw] (c) [below right of=a] {C};

        % Arrows
        \draw[->] (a) -- node[above, align=center] {} (b);
        \draw[->] (a) -- node[above, align=center] {} (c);
        \draw[->] (b) -- node[above, align=center] {} (c);
        \draw[->] (c) -- node[above, align=center] {} (a);
    \end{tikzpicture}
    \label{fig:page-rank}
\end{figure}

Since the surfer randomly chooses between links on a page, if they start at page A, there is a 50\% chance they'll go to page C\@.
Another way of saying this is that the PageRank for a page is divided evenly between all the outgoing links.

This means that the PageRank of page C, represented as $PR(C)$ can be calculated as follows:

\[
    PR(C) = \frac{PR(A)}{2} + \frac{PR(B)}{1}
\]

More generally, we can calculate the PageRank of a page $u$ as:

\[
  PR(u) = \sum_{v \in B_u}^{} \frac{PR(v)}{L_v}
\]

where $B_u$ is the set of pages that point to $u$, and $L_v$ is the number of outgoing links from page $v$.

There's an obvious problem with the example above, in that we don't know the PageRank of the other pages yet.
If we start by assuming that the PageRank values for all pages are the same ($0.\bar{3}$ in this case), then it's easy to see that we can perform multiple iterations.

For example, we could perform the following iterations:

\begin{flalign*}
    PR(C) &= \frac{0.33}{2} + \frac{0.33}{1}, PR(A) = 0.33, PR(B) = 0.17 \\
    PR(C) &= \frac{0.33}{2} + \frac{0.17}{1}, PR(A) = 0.5, PR(B) = 0.17 \\
\end{flalign*}

After a few more iterations, we converge to the final values:

\[
    PR(C) = 0.4, PR(A) = 0.4, PR(B) = 0.2
\]

We can also introduce the ``Surprise Me'' button to the browser.
This will jump to a random webpage when clicked.

This allows us to guarantee that eventually every page on the internet will be reached.
Without the button, we could get stuck on pages which don't point to any others, or on pages that form loops.
Each time a page is visited, we choose whether to follow a link or press the ``Surprise Me'' button.

The change of clicking this button is determined by a probability value, $\lambda$.
We assume that $\lambda$ is very small, so the surfer is much more likely to click a link than press the button.

We can generate a random number $r$, $0 \leq r \leq 1$, and if it's less than $\lambda$, then we click the button, otherwise we follow a link.

If we take the surprise button into account, part of the PageRank for any page will be due to the chance of coming from the button press.
In our scenario above, given there's 3 pages, there is a one-third chance of going to any of the pages when we push the button, and that the chance of pushing the button is $\lambda$, the contribution of the button push to the PageRank is $\frac{\lambda}{3}$.

This gives us the following formula for PageRank:

\[
    PR(u) = \frac{\lambda}{N} + (1-\lambda) \cdot \sum_{v \in B_u}^{} \frac{PR(v)}{L_v}
\]

where $N$ is the number of pages being considered.
A typical value for $\lambda$ is $0.15$.
