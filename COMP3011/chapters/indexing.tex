\section*{Indexing}

Documents are written in natural language, which is difficult for computers to analyse directly.
For this reason, the text is transformed into document features.
A document feature is some attribute of the document we can express numerically.
There are two kinds of features -- topical features estimate the degree to which the document is about a subject, and quality features, for example the number of pages that link to this document or the number of days since the page was updated.
Quality features don't address whether the document is a good match for a query, but address its quality -- a page with no incoming links that hasn't been edited in years is probably a bad match for any query.

We assume that a ranking function $R$ takes the following form:

\[
    R(Q,D) = \sum_{i}^{} g_i(Q)f_i(D)
\]

Here, $f_i$ is some feature function that extract a number from the document text, and $g_i$ is a similar feature function which extracts a value from the query.

\subsection*{Inverted Indices}

All modern search engine indices are based on inverted indices.
An inverted index is the computational equivalent of the index in the back of a textbook.
The index is `inverted' because usually words are part of documents, but instead we have documents associated with their words.

Index terms are often alphabetised, but they don't need to be because a hash table is normally used.
Each index term has its own inverted list that holds the relevant data for the term.
For a book index, the relevant data is the page numbers.
In a search engine, this might be the list of documents.

Each entry in an index is called a posting, and the part of a posting which refers to a specific document is often called a pointer.
Each document in the collection is given a unique number to make it efficient to store document pointers.

Indices in books store more than just the location information.
Usually for important words, one of the page numbers is in bold, indicating that the page has a definition or extended discussion.
Inverted indices can also have extra information, where postings can contain a range of information other than just location.
By storing the correct information, feature functions can be easily computed.

\subsubsection*{Documents}

The simplest form of an inverted index stores just the documents that contain each word, and no additional information.
These indices don't store the number of times that words appear, but rather only the documents themselves.
Inverted lists become more interesting when we consider their intersection, for example sentences that contain two terms.

\subsubsection*{Counts}

In this method, each posting also contains a second number, which is the number of times a word appears in the document.
In general, word counts can be a powerful predictor of document relevance.
Word counts can help distinguish documents that are about a particular subject from those that discuss the subject in passing.

\subsubsection*{Positions}

When looking for queries with multiple words, the location of the words in the document is an important predictor of relevance.
To do this, we can add positional information to the index.
Then, each posting will contain two numbers -- a document number, followed by a word position.
By aligning the posting lists, we can find the best match for a certain phrase.

\subsubsection*{Fields and Extents}

Real documents aren't just lists of words.
They have sentences and paragraphs that separate concepts into logical units.
Some documents have titles and headings that provide short summaries of the rest of the content.
The is where extent lists come in.
An extent is a contiguous region of a document.
We can represent these using word positions.
For example, if the title of a book started on the first word and ended on the 8th word, this could be encoded as $(5,9)$.

\subsubsection*{Scoring}

In the inverted lists are going to be used to generate feature function values, why not just store the value of the feature function?
This approach makes it possible to store feature function values that would be too computationally intensive to compute during the query processing phase.
Storing scores like this can both increase and decrease the system's flexibility.
It increases it because computationally intensive scoring becomes possible, but flexibility is lost in that we can no longer change the scoring mechanism once the index has been built.
In this model, word proximity is removed, meaning we can't include phrase information in the scoring unless we build lists for the phrases too.
These lists require considerable extra space.

\subsubsection*{Ordering}

So far we have assumed that the postings of each inverted list would be ordered by document.
Although this is the most popular option, it's not the only way to order an inverted list.
It can also be ordered by score, so that the highest scores come first.
This makes sense only when the lists already store the score, or when only one kind of score is likely to be computed.
By storing scores instead of documents, the processing engine can focus only on the top part of each list, where the high-scoring documents are stored.

\subsubsection*{Inverted Files}

An inverted file is just a collection of inverted lists.
To search the index, some kind of data structure is necessary to find the inverted list for a particular term.
The simplest way to solve this problem is to store each inverted list as a separate file, where each file is named after the corresponding search term.

Document collections can have millions of unique words, and most of these words will only occur a few times.
This means that an index using files would end up with millions of files, most of which are very small.
A file system typically reserves a few KB for each file, even though most will contain only a few bytes of data.
This results in a huge amount of wasted space.

To fix these problems, inverted lists are usually stored together in a single file, which explains the name.
An additional structure, called the vocabulary or lexicon, contains a lookup table from index terms to the byte offset of the list in the file.
In many cases, this vocabulary lookup table will be small enough to be stored in memory.
In this case, the vocabulary data can be stored in any reasonable way on the disk, and loaded into a hash table when the search engine starts.
If the engine needs to handle larger vocabularies, some kind of tree-based data structure should be used to minimse disk accesses.
