\section*{REST}

Representational State Transfer (REST) was coined by Roy Fielding in his PhD thesis about network-based software architecture.

\subsection*{Representational}

This term refers to how data is represented in transit (from client to server or vice versa).
The representation of data may not match how it's stored on the server/client.
For example, data stored on a server as a CSV file could be transmitted in JSON, HTML, or XML\@.

\subsection*{State}

This describes the state of an entity, for example, learning something new would change your state.
When a client application (e.g.\ a browser) runs, the client initially knows nothing, but as it interacts with a server its state changes.

\subsection*{Transfer}

This represents the transfer of a representation from server to client.

\subsection*{Meaning}

The term REST means the change in the state of an entity, driven by a representation transferred to it from another entity.
This is what happens when we browse a website -- initially we only know the root URL, then as we follow links to other pages, we learn more information.

REST is widely used as a term, and often abused.
People tend to use it for any API that doesn't use SOAP\@.
However, for an API to be considered RESTful, it must satisfy a number of design constraints.
Many APIs that claim to be RESTful only satisfy some of these constraints.

\subsection*{API vs Web Service}

The term Web Service was used to describe early SOAP-based services.
Then, the term became so coupled with SOAP that when SOAP became less popular, so did Web Services as a term.
This is why today it's more common to use the term RESTful API instead of RESTful Web Service, but both are correct.

\subsection*{REST Constraints}

\textbf{Client-Server Architecture}: This is the most obvious, and is satisfied by every web application.
In the early days of the web, this wasn't the case.
Following this improves portability of the user interface, and allows components to evolve independently.

\textbf{Statelessness}: This constraint is met when each request contains all the information needed to understand it, and doesn't rely on previous requests.
This improves visibility because a monitoring system only needs to look at single requests.
It improves reliability because we can recover from partial failure.
It simplifies implementation because state doesn't need to be tracked between requests.
It can however decrease network performance because of the repetitive data that needs to be sent.
Finally, placing the application state on the client-side reduces server control.

\textbf{Cacheability}: The constraint requires that data within a response be labelled as cacheable or non-cacheable (whether implicitly or explicitly).
If a response is cacheable, then a client cache is given the right to re-use the response data for later requests.
This can eliminate some interactions which improves performance.
However, it can reduce reliability of state data in the cache differs from the server state.

\textbf{Uniform Interface}: This constraint can be split into 4 sub-constraints.
\textbf{Identification of Resource} means that each resource must be identified by a unique and stable identifier.
Unique means each should point to one and only one resource.
Stable means it should not be altered later on.
\textbf{Manipulations of resources through representations} means that the information contained in a representation is sufficient to modify the resource.
\textbf{Self-descriptive messages} means that messages should carry all the information the receiving party needs to process it successfully.
\textbf{Hypermedia As The Engine Of Application State (HATEOAS)} means that a representation returned from the server should contain hyperlinks to perform further actions or to access more information about the same/related resources.
This is the driving principle of the World Wide Web.

\subsection*{Layered System}

The REST constraints allow the implementation of a layered architecture, in which the API implementation can sit on one server, but source its data from a separate database, or perform authentication from yet another server, for example.
From the client's perspective, it connects directly with the end server and has no knowledge of any intermediaries.
Each layer is only away of the next layer, and not of those beyond that.

A layered system can boost scalability because tasks such as storage security, request handling, are separated between different servers, without adding complexity for the client.
However, it can add latency because each request needs to go through different layers.

REST works in cycles in which the client requests representations, parses these, recognises structures that lead to further resources, decides on the next resource to request, and then updates their current state.

\subsection*{The Semantic Gap}

Most of the steps above are easy to achieve, however deciding on a resource to request is difficult to implement in automatic code.
This is because it requires the client to have knowledge of the application domain.
This is referred to as the \textbf{semantic gap}.

The Web works because human beings make decisions about which links to click and what forms to fill out.
Every time a browser renders a page, a human has to look at it and decide what to do.
The point of web APIs is to perform tasks without a human having to make decisions.
This is the biggest challenge in web API design -- bridging the semantic gap between understanding a document's structure and understanding what it means.
