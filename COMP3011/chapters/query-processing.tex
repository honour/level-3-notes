\section*{Query Processing}

Once an index is built, we need to process the data in it to produce query results.
Clever search algorithms can boost query processing speed by 10--100x over simpler versions.
There are two simple query processing techniques -- document-at-a-time, and term-at-a-time.

\subsection*{Document-at-a-Time}

Document-at-a-time retrieval is the simplest way to perform a retrieval with an inverted file.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=\linewidth]{assets/document-at-a-time}
    \label{fig:daat}
\end{figure}

The figure shows a retrieval for ``salt water tropical''.
The inverted lists are horizontal, and each column represents a different document.
The inverted list holds word counts, and the score the sum of the word counts in each document.
To begin, the counts for the first document are added to get the score for the document.
Once that's completed the next document is scored, etc.

\textbf{Advantages:} The primary benefit if its economical use of memory.
The major use of memory comes from a priority queue.
We can also skip documents that don't appear in any of the inverted lists.

\subsection*{Term-at-a-Time}

Instead of going through each document at a time and computing a final score, each term is iterated and a partial score is created.
These are them accumulated together to form the final score.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.8\linewidth]{assets/term-at-a-time}
    \label{fig:taat}
\end{figure}

The disadvantage of this method is the memory usage of an accumulator table.
Term at a time uses more efficient disk access though.
Because it reads each list from start to finish, it requires minimal disk seeking.

\subsection*{Conjunctive Processing}

One of the simplest kinds of query optimisation is conjunctive processing.
Conjunctive processing means that every document returned to the user must have all of the query terms.
This is the default mode for many search engines, because it's quick, and users have come to expect it.
With short queries, conjunctive processing can improve efficiency, but search engines that use longer queries, like full paragraphs, will not perform will with conjunctive processing.

Conjunctive processing works best when one of the terms in the query is rare.
Since we're only interested in documents that contain all words, it's quick to skip over words in the most common list until we find ones that match the rarer terms.
Conjunctive processing can be used on both document-at-a-time and term-at-a-time systems.

For many queries, we don't need all of the information stored in an inverted list.
Instead, it can be more efficient to just read the small portion of data relevant to the query.
For example, with a query like ``galago AND animal'', animal occurs in 300 million documents compared to 1 million for galago (on the web).
If the inverted lists are in document order, we can use the following formula:

\begin{enumerate}
    \itemsep-0.75em
    \item Let $d_g$ be the first document in the list for galago
    \item Let $d_a$ be the first document in the list for animal
    \item While there are still documents in the list for galago and animal, do the following:
    \begin{itemize}
        \itemsep-0.75em
        \item If $d_a = d_g$, the document contains both terms, so move both pointers to the next documents in their respective lists
        \item If $d_a < d_g$, set $d_a$ to the next document in the animal list
        \item If $d_g < d_a$, set $d_g$ to the next document in the galago list
    \end{itemize}
\end{enumerate}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=\linewidth]{assets/conjunctive-processing}
    \label{fig:conjunctive-processing}
    \caption*{Conjunctive Processing Example}
\end{figure}

\subsubsection*{List Skipping}

The algorithm explained above is very expensive.
It processes almost all documents in both lists, so for the above example it would have to go through nearly 300 million documents.
The vast majority of the processing time would be spent on the documents which contain the more popular term (animal in this case).
We can change the algorithm slightly to skip forward in the animal list.
Every time that $d_a < d_g$, we can skip ahead $k$ documents to a new document, $s_a$.
If $s_a < d_g$, we skip ahead again.
We can do this until $s_a \geq d_g$.
At this stage, we have narrowed the search down to a range of $k$ documents that might contain $d_g$, which is a simpler search.

We can achieve list skipping using \textbf{skip pointers}.
Skipping doesn't actually improve the asymptotic running time of reading a list.
Suppose we have an inverted list that's $n$ bytes long, but add skip pointers after every $c$ bytes, and these pointers are $k$ bytes long.

Reading the whole list requires reading $n$ bytes, but jumping through using the skip pointers requires $\mathcal{O}(kn/c)$ time, which simplifies to $\mathcal{O}(n)$.
Even though we don't have an asymptotic gain, the factor of $c$ can be huge.
For typical values ($c = 100, k = 4$), skipping through a list results in reading just 2.5\% of the total data.

In $kn/c$, as $c$ gets bigger, the amount of data you need to read to skip through the list decreases.
So why wouldn't we make $c$ as large as possible?

The problem is that if $c$ gets too large, the average performance drops.
Suppose we want to find $p$ postings in an inverted list, and the list in $n$ bytes long, with $k$-byte skip pointers at $c$-byte intervals.
Therefore, there are $n/c$ total intervals in the list.
To find those $p$ postings, we need to read $kn/c$ bytes in skip pointers, but we also need to read data in $p$ intervals.
On average, we assume that the postings we want are about halfway between two skip pointers, so we need to read an additional $pc/2$ bytes to find those postings.
The total number of bytes read then equals:

\[
    \frac{kn}{c} + \frac{pc}{2}
\]

This shows that whilst a larger value of $c$ makes the first term smaller, it also makes the second term larger.
Therefore, picking the perfect value of $c$ depends on the value of $p$, and we don't know what $p$ is until a query is made.

It's possible to use previous queries to simulate the skipping behaviour, allowing a good estimate for $c$.
