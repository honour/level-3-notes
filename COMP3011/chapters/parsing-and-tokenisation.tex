\section*{Parsing and Tokenisation}

For search engines to work correctly, they must recognise both the content and structure of text documents.
A tokeniser, or lexical analyser, extracts the words from a sequence of characters in a document.
A parser, or syntactic analyser, uses tags and other metadata in a document to interpret the document's structure, based on the syntax of the markup language.
In addition to the natural language, there can be lots of other content in a document, such as metadata, images, and tables.

Metadata is information about a document that isn't part of the text, such as document attributes (like date and author) and the tags that are used by markup languages.
In most cases, there is more metadata than useful data in a document.

\subsection*{Tokenisation}

Tokenising is the process of forming words from the sequence of characters in a document.

In many early systems, a word was defined as any sequence of alphanumeric characters of length 3 or more, terminated by a space or other special character.
All uppercase letters were also converted to lowercase.
However, this simple process is not adequate for most search applications because too much information is discarded.

Small words can be important in some queries, usually in combination with other words.
Both hyphenated and non-hyphenated forms of many words are common.
In some cases, the hyphen is not needed.
At other times, hyphens should be considered either as part of the word, or a separator.
Special characters are an important part of the tags, URLs, code, and other important parts of documents that must be correctly tokenised.
Capitalised words cna have different meaning from lowercase words.
Apostrophes can be a part of a word, a part of a possessive, or just a mistake.
Numbers can be important, including decimals.
Full stops can occur in numbers, abbreviations, URLs, ends of sentences, and other situations.

\subsection*{Parsing}

A parser uses the tags and other metadata recognised in the document to interpret the document's structure based on the syntax of the markup language.
The parser then produces a language agnostic representation of the document that includes both the structure and content.
For example, an HTML parser can interpret the structure of a web page, and creates a DOM representation of the page that is used by a web browser.

\subsubsection*{Document Structure and Markup}

Some parts of the structure of webpages, indicated by HTML markup, are very significant for ranking pages.
The document parser must recognise this structure and make it available for indexing.
The main heading for the page indicates that it is particularly important.
If the same phrase is also in bold and italics in the body of the text, then this is further evidence of importance.
Other words and phrases are used as the anchor text for links, and are likely to be good terms to represent the content of the page.

\subsubsection*{Two-Pass Tokenisation}

The tokenising process can be divided into two passes.
In the first pass, we focus entirely on identifying markup or tags in the document.
This could be down using a tokeniser and a parser designed for the specific markup language used.
One such HTML parser in Python is called \textbf{BeautifulSoup}.
It's a library for pulling data from HTML and XML documents.

In the second pass, we focus on the appropriate parts of the document structure (like headings, body text, etc.).
Parts that are not useful for searching, such as those containing HTML code, are ignored in this second pass.

\subsubsection*{Stopwords}

Human language is filled with function words, that is, words that have little meaning in isolation.
They are called stopwords, because text processing usually stops when one is seen, and the word is discarded.
These words are part of how we describe nouns in text, and express concepts like location and quantity, e.g.\ over, under, below.
These words are very common in English, but they rarely indicate anything about the document relevance alone.

If we are considering individual words in the retrieval process, and not phrases, these words have very little use.
Throwing out these words decreases index size, increases retrieval efficiency, and generally improves effectiveness.

However, removing too many of these words will negatively impact retrieval.
For instance, the query ``to be or not to be'' consists purely of what are usually stopwords.

\subsubsection*{Stemming}

Stemming, also called conflation, is a component of text processing that captures the relationships between different variations of a word.
Stemming reduces the different forms of a word that occur because of inflection or derivation to a common stem,
For example `swimming' and `swam' would be reduced to the same stem -- swim.
This allows the search engine to determine that there's a match between words, even if it's not exact.

In English, stemming produced a small but noticeable improvement, but in very inflected languages like Arabic and Russian, stemming is crucial.

An algorithmic stemmer uses a program to decide whether two words are related, usually based on knowledge of word suffixes for a particular language.
A dictionary-based stemmer has no logic of its own, but relied on pre-created dictionaries of related terms to store the relationships.
In these stemmers, the related words don't even need to look similar (such as for verbs like ``is'', ``be'' and ``was'').

One of the most popular algorithmic stemmers is the Porter stemmer (from the 1970s).
It consists of a number of steps, each containing a set of rules for suffix removal.
At each step, the longest applicable suffix is removed.
The original version made a number of errors, but a new version, Porter2, fixes some of these problems.
It's available for a number of languages.

Some languages are highly inflectional, which means the root word can have many variants.
A stemming algorithm that reduced Arabic words to their roots wouldn't help in search, so a broad range of prefixes and suffixes must be considered.

In these highly inflectional languages, proper stemming can make a large difference to the accuracy of the ranking.
An Arabic search engine with high quality stemming can be more than 50\% more effective at finding relevant documents compared to no stemming.
In contrast, the improvement varies from around 5--10\%.

\subsubsection*{Part-of-Speech (POS) taggers}

Many of the two and three word queries submitted to search engines are phrases, and finding documents that contain those phrases will be part of any effective ranking system.
Phrases are more precise than single words as descriptions, and are usually less ambiguous.
The impact of phrases on retrieval can be complex, for example given a query like ``fishing supplies'', should the retrieved documents contain exactly that phrase, or should they get credit for containing the words ``fish'', ``fishing'' and ``supplies'' in the same paragraph/document?
The definition of a phrase that is used most frequently in information retrieval is that a phrase is equivalent to a simple noun phrase.
This is often restricted even further to include just sequences of nouns, or adjectives followed by nouns.
Phrases defined by these criteria can be found using a POS tagger.

A POS tagger marks the words in a text with labels corresponding to the part-of-speech of the word in that context.
Taggers are based on statistical or rule-based approaches and are trained using large corpora that have been manually labelled.
Typical tags that are used include NN (singular noun), NNS (plural noun), VB (verb), etc.
