\section*{Bridging the Semantic Gap}

An example of bridging the cap would be the design of a web API for navigating mazes.
The server's job will be to invent mazes, and present them to clients.

This problem has been chosen because any complex problem can be represented as a hypermedia maze that a client must navigate.
For example, the problem must be broken into smaller steps, every client begins at the same step, and at each step in the process a client must choose what step to take next.

\subsection*{The Media Type}

To represent a maze in a machine-readable format, a personal standard called \texttt{Maze+XML} has been created.
The MIME-type of a Maze+XML document is \texttt{application/vnd.amundsen.maze+xml}.

A suffix is an augmentation to a media type to specify the underlying structure.
For example, Maze+XML means that the media type uses XML syntax, but with its own defined semantics.
Structured syntax suffix types are registered and maintained by IANA\@.
Currently registered suffixes include \texttt{xml}, \texttt{json}, \texttt{ber}, \texttt{der}, and more.

The Maze+XML media type is one way a domain-specific design meets the semantic challenge, by defining a document format to represent the problem and registering a media type for that format (so clients know when they've encountered an instance of this problem).
In general, it's not recommended to create domain-specific media types.
It's usually less work to add application semantics to a generic format.
Inventing your own results in fiat standards which don't take advantage of the previous work of others.

\subsection*{Fiat Standards}

Fiat standards aren't actually standards, but rather a description of how one person does things.
The behaviour might be documented, but the core assumption of a standard is that other people ought to do things the same way.
This is missing with fiat standards.
Most APIs today are fiat standards -- one off designs associated with a specific company.
Even under ideal circumstances, your API will be a fiat standard, since business requirements will differ from other people's.
Ideally, your fiat standard will just be a light gloss over a number of other standards.

\subsection*{Link Relations}

Standards can meet the semantic challenge by defining link relations that convey the application semantics.
A link relation is a string associated with a hypermedia control.
It explains the change in application state or resource state that will happen if that control is triggered.
Link relations are managed by IANA\@.
Currently, there are about 60 link relations that have been deemed to be generally useful.
The simplest examples are \texttt{next} and \texttt{previous} for navigating lists.
RFC 5988 defines two kinds of link relation -- registered and extension types.
Registered link relations look like the ones in the IANA registry.
Extension relations look like URLs.
This allows people to define relations that mean whatever they want.

\subsection*{Meeting the Semantic Challenge}

For the designer of a domain-specific API, bridging the semantic gap is a two stage process.
First we need to write down the application semantics in a human-readable specification.
Then, we should register one or more IANA media types for the design.

Client developers can then reverse the process for bridging the gap in the other direction.
They can look up the media type in the IANA registry, and then read the specification to learn how to deal with that media type.

There is no magic shortcut -- to get working client code users will have to read your documentation and do some work.
We can't completely eliminate the semantic gap because computers can make deductions that humans can.
