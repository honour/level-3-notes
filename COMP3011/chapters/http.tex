\section*{HTTP}

HTTP is an application layer protocol which is at the core of web communication.

Each web resource must be given a name, so clients can find resources.
The resource name is called a \textbf{Uniform Resource Identifier (URI)}.
The general format of URIs is \texttt{scheme://host/path}.

\subsection*{HTTP Format}

All HTTP messages are either requests or responses.
They contain a start line describing the message, a block of headers containing some attributes, and some optional body of data.
Each line is terminated with a \texttt{CRLF} sequence.

\texttt{GET /test HTTP/1.1 \\ Accept: */* \\ Host: xela.tech}

\subsubsection*{Request Message Format}

\texttt{<METHOD> <PATH> <VERSION> \\ <HEADERS> \\ \\ <BODY>}

\subsubsection*{Response Message Format}

\texttt{<VERSION> <STATUS> <REASON> \\ <HEADERS> \\ \\ <BODY>}

Headers follow the format \texttt{key: value\textbackslash r\textbackslash n}.
The headers are separated from the request/response body with an additional CRLF sequence.

\subsubsection*{Header Types}

\textbf{General} headers are used by both clients and servers, for example the Date header.
\textbf{Request} headers are specific to request messages.
These provide extra information to servers, for example the Accept header, which indicates what format the client can accept a response in.
\textbf{Response} headers provide information to the client, for example the Server header to indicate what application is running.
\textbf{Entity} headers refer to headers that deal with the entity body.
For example the Content-Type header lets the application know that the data is in a particular format.

\subsubsection*{Status Codes}

Status codes are three-digit numbers describing the result of a request.
The first digit specifies the category the status is in, of which there are 5: 1XX - Informational; 2XX - Success; 3XX - Redirection; 4XX - Client Error; 5XX - Server Error.

\subsection*{MIME Types}

In HTTP, the Content-Type and Accept headers use Multipurpose Internet Mail Extensions (MIME) types.
These are labels which identify the format of data.
These were originally designed for moving files between mail systems, and was adopted by HTTP\@.

Each MIME type consists of a primary type, a subtype, and a list of optional parameters.
The type and subtype are separated by a slash, and optional parameters begin with a semicolon.
The primary type can be predefined, an IETF defined extension token, or an experimental token (starting with \texttt{x-}).
Examples include \texttt{application/json}, \texttt{application/vnd.ms-powerpoint}, and \texttt{text/plain}.

MIME types should be registered with IANA, and the registration is open to everyone.
MIME type tokens are split into four classes, called \textbf{registration trees}.
These are \textbf{IETF}, \textbf{Vendor (.vnd)}, \textbf{Personal (.prs)}, and \textbf{Experimental (x-/x.)}.

\subsection*{HTTP Methods}

\textbf{Note:} Not all HTTP methods have to be implemented by every server.
Compliance with HTTP/1.1 only requires \texttt{GET} and \texttt{HEAD}.

\textbf{GET} is the most common method, usually for fetching a resource from a server.

\textbf{HEAD} behaves just like GET, but only the headers are returned.
No response body is ever returned.
This allows clients to inspect headers without the whole resource having to be transferred.

\textbf{PUT} writes documents to a server, in the opposite way that GET reads documents from a server.

\textbf{POST} sends input data to the server, for example with HTML forms.

\textbf{TRACE}: When clients make requests, it may have to travel via firewalls, proxies, and gateways.
Each of these has the opportunity to modify the original request.
The TRACE method allows clients to see how the request looks when it finally reaches the server.

\textbf{OPTIONS} asks the server to inform about the various capabilities of the server.
For example, the methods supported for a particular resource.

\textbf{DELETE} allows a resource at a particular URI to be removed.
However, the client application doesn't get a guarantee that the resource will be deleted.

\subsubsection*{Extension Methods}

HTTP is designed to be extensible, and extension methods are those that are not defined in HTTP/1.1.
This provides developers with the ability to extend the capabilities of HTTP for their applications.
