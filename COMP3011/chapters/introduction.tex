Web Services appeared as a result of the need to build software systems with components that are distributed across the web.
For example, an e-commerce site might need live exchange rates to sell products.
The system that requests data is a \textbf{service requester}, the software that provides the data is a \textbf{service provider}.

Web services allow us to create \textbf{mashups}.
A \textbf{mashup} is a software that uses content from more than one source to create a single, new service.

The main challenge with web services is that different system have varying operating systems, programming languages and platforms.
Many common data formats like XML and JSON are recognised by the vast majority of software, so are ideal for the web.
Existing technology like HTTP can be used to leverage machine-to-machine communication.

\subsection*{Web Services}

\subsubsection*{SOAP-based Services}

In SOAP, rules for communication must be defined, such as:

\begin{itemize}
    \itemsep-0.75em
    \item How one system can request data from another
    \item Which parameters are needed
    \item The structure of the data produced
    \item What error messages to display
\end{itemize}

All of these rules are described in a file in the Web Services Description Language (WSDL) format.
A directory called UDDI (Universal Description, Discovery and Integration) defines which software system should be contacted for which data.
Once the client selects the system to contact, it does so using SOAP\@.

\subsubsection*{RESTful Web Services}

A RESTful web API is a development in web services where emphasis has moved to simpler REST-based communications.
Unlike SOAP, RESTful APIs do not require XML-based protocols (like SOAP and WSDL).
There is no official standard for REST, it is an architectural style, whereas SOAP is a protocol.
Although REST isn't a standard, it makes use of standards like HTTP, URI and JSON/XML\@.

\subsubsection*{REST's Main Features}

In RESTful systems, the server doesn't maintain state information about the client.
Each request is served independently.
THe client is responsible for maintaining its own state based on the representations sent by the server.
REST-compliant services allow requesting systems to access and manipulate \textbf{resources} using a uniform and predefined set of stateless operations.
In a RESTful service, requests made to a resource's URI will trigger a response which may be in XML, HTML, JSON or any other multimedia format.
The response may confirm that some alteration has been made to the resource, and it may provide hypertext links to other related resources.
RESTful Web APIs use HTTP as the underlying protocol, using the predefined GET, POST, PUT, PATCH, and DELETE methods.

\subsubsection*{REST Architectural Constraints}

There are a number of non-functional properties that define a RESTful system, such as performance, scalability, simplicity, modifiability, visibility portability, and reliability.

REST architectural constraints are mainly achieved through:

\begin{itemize}
    \itemsep-0.75em
    \item Client-server architecture: This improves portability of the user interface
    \item Statelessness: The communication is constrained by no client context being stored on the server.
    \item Uniform Interface: By simplifying and decoupling the architecture, enabling each part to evolve independently.
\end{itemize}

\subsubsection*{SOAP vs REST}

\begin{tabular}{|p{0.475\linewidth}|p{0.475\linewidth}|}
    \hline
    \textbf{SOAP} & \textbf{REST} \\
    \hline
    Is a protocol & Is an architectural style \\
    \hline
    Defines standards to be strictly followed & Does not define strict standards \\
    \hline
    Requires more bandwidth and resources & Requires less bandwidth and resources \\
    \hline
    Defines its own security & Inherits security measures from the underlying transport layer \\
    \hline
    Permits the XML data format only & Permits different data formats such as plain text, HTML, XML, JSON etc. \\
    \hline
    Can support stateful implementations & Follows the stateless model \\
    \hline
    SOAP based reads cannot be cached & REST reads can be cached \\
    \hline
    Can be difficult to scale & Has better performance and scalability \\
    \hline
    XML is more difficult to parse & JSON parses much faster than XML \\
    \hline
\end{tabular}

The majority of web services today use REST\@.
Major providers of cloud services are largely phasing out their SOAP-based interfaces.
However, SOAP is still preferred when higher security is required.

\subsection*{Search Engines}

A web search engine is a software system designed to search for information on the WWW\@.
Unlike web directories, which are maintained by human editors, search engines continuously obtain data using web crawlers.
A search engine maintains the following processes in near real-time:

\begin{itemize}
    \itemsep-0.75em
    \item Web Crawling
    \item Indexing
    \item Searching
\end{itemize}

\subsubsection*{The Deep Web}

The deep web is the part of the WWW which cannot be indexed by web search engines.
The content of the deep web is hidden behind things like HTML forms, web mail, paywalls, etc.
Content of the deep web can only be located by a direct URL or IP address, and may require a password or other security to access.

\subsubsection{Indexing}

Indexing means associating words found on web pages to their URLs and HTML-based fields.
The information to be indexed depends on many factors, such as the titles, page content, headings, and metadata in HTML meta tags.
This indexing allows for information related to the query to be found as quickly as possible.

\subsubsection*{Searching}

When a user enters a query into a search engine, the index already has te URLs containing the keywords, and these can be instantly obtained from the index.
The real processing load is generated the list of results, by weighting according to information in the indices.

\subsection*{Linked Data}

The traditional web is a collection of linked documents that have little structure and the data is available in so many formats.
This type of data is formatted for human consumption, and is not easy for automated processes to access.
This makes it largely a human task to analyse the semantic relationships between data in various web pages.

\textbf{Linked Data} refers to a set of techniques for publishing and connection structured data on the Web.

