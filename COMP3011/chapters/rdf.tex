\section*{RDF}

RDF descriptions are comprised of a number of statements, called triples.
Each statement has three parts, a subject, predicate, and an object.

\subsection*{The Subject}

The subject represents the entity being described.
It must be a resolvable URI\@.
Requesting the URL should return information about the subject (entity).

\subsection*{The Predicate}

The predicate determines an attribute of the subject.
It also must have its own resolvable URI\@.
If you need to know what a predicate means, and find other information about it, the URL can be searched in a browser.
Predicates themselves might be defined using RDF statements.

\subsection*{The Object}

The object determines the value of the attribute, or the object of the subject.
It can be a resolvable URI, or a literal.

A URL can be abbreviated into a prefix:suffix, such as \texttt{dc:Creator}.
There are some common prefixes which are part of the standard RDF vocabulary, and commonly used in linked data.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=\linewidth]{assets/rdf-prefixes}
    \caption*{RDF Prefixes}
    \label{fig:rdf-prefixes}
\end{figure}

\subsection*{Blank Nodes}

Blank nodes are useful when you need to link to a collection of items but don't want to bother making up a URI for it.
Some RDF databases automatically assign URIs to blank nodes, so they may be more easily operated on.
This process is called \textbf{skolemisation}.
In general, we should avoid using blank nodes when possible.

\subsection*{Classes}

RDF resources may be divided into groups called classes using the property rdf:type in the RDF Schema (RDFS) standard.
The members of a class are known as instances of the class, just as they are in object-oriented programming.
RDFS classes are themselves RDF resources and are of type rdfs:Class.
The rdfs:subClassOf property may be used to state that one class is a subclass of another.

\subsection*{RDF Vocabulary}

RDF Vocabularies provides definitions of the terms used to make relationships between data elements.
The vocabularies are distributed over the Web, are developed by people from all over the world, and only come into common use in Linked Data if lots of people choose to use them.
There are a number of terms used in Linked Data like foaf:name, rdfs:label, and vcard:locality.
These terms are grouped together to form RDF Vocabularies.

Anyone can create one, and many people do.
This might seem like a recipe for disaster -- how can Linked Data be reused if it contains terms that haven't been seen before.
There are two ways to make the problem tractable -- make certain that the URIs defining Linked Data vocabularies themselves follow Linked Data principles, and to reuse existing vocabularies whenever possible.

\subsubsection*{Guidelines for Minting URIs}

Using a DNS domain that you control is essential, and keys should be easily understood by humans.
The URIs should also be neutral to implementation details (e.g.\ index.aspx is bad practice).

\subsubsection*{Fragment Identifiers}

Fragment identifiers are the part of URLs that follows the hash symbol.
Fragments are often used in HTML pages to point to a specific section in the page.
They are not passed to web servers by clients.
Fragment identifiers are used by many Linked Data vocabularies because the vocabulary is often servers as a document and the fragment addresses a particular term inside it.

\subsection*{RDF Formats for Linked Data}

RDF is a data model, not a format.
Different formats can be used to serialise the RDF data.
Some of the most commonly used formats are Turtle, RDF/XML, RDFa and JSON-LD\@.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=\linewidth]{assets/turtle}
    \caption*{Turtle: Terse RDF Triple Language}
    \label{fig:turtle}
\end{figure}
