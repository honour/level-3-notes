\section*{Evaluation of User-Adaptive Systems}

When evaluating a user-adaptive system, we can either choose to evaluate a system prototype (formative evaluation), or a system that has already been deployed in practice (summative evaluation).
With prototypes, we should be asking whether the system as a whole works as intended, whether each of the components themselves perform adequately, and possibly which design from a set of prototypes should be chosen to continue developing.
Once the system is deployed, we want to look at the broad benefits of adaption (both for the user and business), but also if there are any potential drawbacks of this.

When splitting evaluation into components, we need to evaluate the following:

\begin{enumerate}
    \itemsep-0.75em
    \item Collection of input data
    \item Interpretation of the collected data
    \item Modelling the current state
    \item Deciding upon adaptation
    \item Applying adaptation decisions
    \item Evaluating the adaptation system as a whole
\end{enumerate}

\subsection*{Collection of input data}

The goal of this stage is to evaluate the quality of the raw input data.
Are we collecting the right information about the user?

We can assess this based on accuracy -- Is this the correct user's information?
Are we selecting user's interests properly?

We can also look at latency -- what is the time difference between information collection and when it will be used, are we handling this properly?

Finally, sampling rate is important.
We could be missing information about the user based on when we are sampling data.
Is the data we're capturing applicable to all users, or will they not be available for some?

This can be achieved through a number of methods.
Data mining can be used on log data from the application, and look to identify any anomalies that might indicate inaccuracies.
Cross Validation is similar to data mining, it allows some batches of data to be used for training, and another batch for validation.
It's only used to validate one aspect about the user.
User tests are small tests, where some users are invited to use the application, we can look at what has been captured, and get feedback about the performance.
Simulated users allows dummy people to be created to test generation of user data, and see if the application performs as expected.

\subsection*{User Model Acquisition}

The goal here is to check that input data has been interpreted correctly.

The criteria for this are validity -- is the model that has been generated valid?; predictability -- are we correctly predicting user behaviour?; scrutability -- will the user understand how the system came to the decisions it provides?

We can check this by data mining on historic data, and seeing if predications are being made accurately.
For scrutability, we need to perform users tests.
We can also evaluate with heuristics -- we can make a checklist for scrutability and validity, and ensure that the criteria are being met.
The transparency checklist is an example of this.

\subsection*{User Model}

The goal is to check the constructed user model matches the user.
This is similar to the previous section, but here we are checking for the outcome, rather than the performance of the algorithm.

The same criteria apply, but here scrutability means that we want to check if the user model itself can be understood by the user, and they can know why certain things have been included.

There are also additional criteria for the user model, based on responsible personalisation.
Consciousness -- is the data we are collecting ethically appropriate?
Comprehensiveness -- are we missing things?
Precision/Sensitivity should also be considered.

Additional methods to verify this are focus groups, and the user as a wizard.
Once users have tried the application they can be brought together, to discuss the effectiveness.
When the user is a wizard, they act as the adaptive system, and we can make comparisons into whether the items that the actual user suggests align with what our system will.

\subsection*{User Model Application}

We want to determine whether the adaptation decisions that are made are optimal.
We should check that the content we're showing is appropriate, whether the user is accepting what is shown, whether the interface is being adapted in a way that is predictable (and therefore easy to use), transparency in terms of allowing the user to understand how decisions were made, and providing a breadth of experience -- we don't want to narrow the user too much.

\subsection*{Applying Adaptation Decisions}

Here we want to ascertain whether the implementation of the adaptation decisions is optimal.
The criteria we want to compare against are usability, timeliness, obtrusiveness, acceptance, predictability, and breadth of experience.

We can use the same methods of testing as explained above: heuristic evaluation, cognitive walkthrough, focus groups, user as wizard, and user testing.

\subsection*{The System as a Whole}

This is about evaluating the utility of the system, including both the benefits that it brings, as well as any possible drawbacks (usability threats).

Typical benefits of user adaptive systems include efficiency (performing tasks quicker), effectiveness (performing tasks better), improved user satisfaction, better trust (with users feeling more valued), accessibility (allowing all users to be catered for), and potentially improved revenue and customer retention in e-commerce applications.

As mentioned above, always look at potential usability threats.

\subsection*{AB Testing}

Experimental studies can be performed with users in the form of AB tests.
Here different versions of a system (or different versions of a feature) can be tested with different user groups, and performance and usability metrics can be collected.

We can then compare metrics like the median and mode, as well as using statistical significance tests, such as parametric ones (like T-Tests/ANOVA), and non-parametric options.
