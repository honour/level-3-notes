\section*{User Model Representation and Building}

All of this section covers the left half of the schema: the information about the User, acquiring the user model, and the model itself.

\subsection*{User Information Collection}

Information can be collected about the user in an explicit or implicit manner.

Explicit information is entered by the user themselves, and most commonly is provided through forms/wizards, in a self-report/self-assessment.
This is fairly reliable, complies with privacy regulations, and gives the user control over how much data they share.
However, it requires the time and willingness of users to share the required information, can be obtrusive, and doesn't necessarily pick up dynamic changes in the information.

Implicit information is collected by the system, using digital traces of user information.
It can also include information that has been captured by the user's device.
Below is a number of different methods for implicit data collection.

\textbf{Browser Cache}: Through the use of cookies, browsing behaviour and relationships with websites can be tracked.
Cookies are easy to implement, and widely used, however the accuracy of information can be poor and users can choose to opt out.

\textbf{Interaction/Desktop Agents}: These are small programs which reside on the user's computer, collect information about them, and share it back with the server.
These allow more control over implementation, the user confirms on login/registration and is aware of the collection, and the information can be accessed from different devices and is usually quite accurate.
However, users may be put off by the presence of the agents, and possibly abandon the applications.

\textbf{Web/Search Logs}: These collect browsing/search activity and store information about the user per-visit.
These can be reliable for the short term, and use a session-based model.
It doesn't require storing any personal information, but they are not reliable for long term adaption.

\textbf{File Transfer}: This involves passing the user model from one system to a new one.
It allows for broad information to be collected about the user, is non-obtrusive, and can be used to collect reliable information that was previously gathered explicitly.
However, there are potential privacy concerns, conflicting information needs to be handled, and there may be concerns about the relevance and reliability of the information from another system.

\textbf{Mobile/Wearable Sensors}: These are devices placed on the user which collect data whilst they are being worn.
They allow for in-situ information collection, with a wide range of signals available.
They are growing in popularity and are accessible and readily available.
However, there are potential privacy violations in regards to the data being collected constantly, the data may be noisy/unreliable, and there is a significant amount of multi-faceted data being generated, which may be hard to translate into meaningful adaptions.

An emerging option is \textbf{speech/social media interactions}.
This can provide rich information about the user, including sentiments and viewpoints, and can provide insights in hard to otherwise capture areas, like emotions and personality.
However, gathering information from social media posts may concern users in terms of privacy, the data can be noisy, and may lead to a poor quality model.

Due to the different methods having various pros and cons, lots of systems will combine different collection methods, explicit and implicit, to maximise the quality of the information they gather on the user.

\subsection*{User Model Representation}

A variety of facets can be modelled in a user profile/model: Interests, Knowledge, Goals \& Tasks, Background, Individual Traits, and Context.

\subsubsection*{Representing Interests}

Interests are mainly included in adaptive information-oriented systems.
These can be represented in a number of ways, explained below.

\textbf{Keywords}: This is just a list of words, which can be fairly reliable, will capture the user's view of content, can be specific and capture niche interests, and don't have to be limited by the system.
However, the interests may not map very well to the content being recommended, there's no linking between different keywords, and it can be hard to find relationships in order to recommend similar content.

\textbf{Overlay on known words}: Instead of just using a list of any number of words, this uses a set of words known by the system.
This means that content can more easily be mapped to interests, and for recommendations to be linked to existing content.
However, there is still no linking between keywords, and it can be hard to find relationships to recommend similar content.

\textbf{Graph of interests}: Here, different interests can be connected in a structure to enable the representation of relationships between them.
This makes it easier to find categories and subcategories, it's concept based in terms of relations because it allows grouping content, and it allows content to be tagged with more than one category, so it can be found from multiple locations.
However, things (like the user's view/content) maybe not fit the structure, possibly leading to confusion or for content to be missed out.
Also, the quality of what is recommended will depend on the quality of the graph.

\subsubsection*{Representing Knowledge/Skills}

Knowledge/skills can be represented using a scalar model, like a mapping of these facets to their value, for example \texttt{Physics Level: Novice, Beginner, Intermediate, Expert}, or \texttt{Cooking Experience: 0, 1, 2, 3, 4+ Years}.
This is simple to implement, however the boundaries of categories may not be clear, it can be hard to get granularity correct, and variations of the scale may not be catered for.

A graph structure can also be used instead.
Here, a graph can be created of the domain knowledge (or expected expertise), and a model for the user can be overlaid onto this (we can mark off which parts of the graph the user aligns with).
This method however is dependent on the quality of the initial knowledge model.

\subsubsection*{Representing Goals/Tasks}

This is the most challenging, and recognition of these is difficult.
Goals and tasks are usually represented with a catalogue, that is, a pre-defined list/structure of user goals or tasks.
The user's current goal is then marked in the catalogue, and can then be used for appropriate adaption.
This commonly occurs in learning systems.

For example, there could be a list format, where goals of the user are marked as complete as they progress, or as a graph structure, where the user's completion can be overlaid, optionally including a probability if recognition of the goals' completion is imprecise.

\subsubsection*{Background}

Relatively stable set of user information about their previous experience.
This could include things like their profession, experience in certain areas, etc.
It could also include things like demographic information, like name, sex, age, nationality, etc.

\subsubsection*{Individual Traits}

Information which defines the user as an individual.
This may include things like cognitive style (holistic vs verbal-imagery).
These can be quite hard to capture, and there's no black and white categories here.
Learning style can also be a trait included here (e.g.\ audio, video, reading).
Personality is another example, however most people don't just fall clearly into a single category, but finding this information out can often help with adaption.

\subsubsection*{Context}

We might need to capture this information in some scenarios, often with mobile devices.
This can include facets like location, physical environment, social context, and possible affective state (although this one can be difficult).
While this isn't completely about the user, addition of context can make adaption more effective (but this isn't included in the user model, and will change over time).

\subsubsection*{Summary}

User models are often stored in a database, or also common is XML files.
Not all parameters will be necessary for a certain application, and the user model should only contain those that are needed for adaptation, and require appropriate mechanisms for capturing these.

\subsection*{User Model Building}

We need to take the input information about the user, taking into account the user model representation, and conduct appropriate processing to extract a user model from this information.

\subsubsection*{Click-through based approaches}

Software will expose you to items, and sometimes you'll click on them, and maybe you wouldn't.
Items may be repeated in different sections too.
We collect measurements about how users interact with these items, for example clicks, time spent viewing each item, or whether they are placed in favourites etc.
This allows for metrics to be built, possibly by combining different measurements,
These metrics are proxies for behaviours which are then included in the user model.

For example for clicks, we can measure the number of times an item has been seen, and how many times the user has clicked it.
This allows us to define a metric of `click-through rate'.
This is simply the percentage of times an item has been clicked against the amount of times it has been shown to the user.
We can then use this rate to determine a user's level of interest/engagement for specific items, by selecting an appropriate classifier to do this.

\subsubsection*{Building Keyword-based User Models}

In a scenario where we have a set of documents, and we've used a metric to determine the documents which the user have responded positively to, we need to extract keywords from these and apply a weighting to them.
This is most often done with TFIDF (Term Frequency Inverse Document Frequency).
Inside the documents we count the frequency of the terms (keywords), and document frequency refers to the frequency of the documents themselves.
Once the keywords have been extracted and weighted, they can then be represented as a vector.
The user interests can then be compared with the document vectors to find matches/recommendations, represented in the user model in the form of interest vectors.

\subsubsection*{Building Graph-based User Models}

In this scenario the user is exposed to some documents, and we use a metric to identify positive examples.
We then use semantic tagging to link these documents to the graph structure.
We can then apply processing, such as considering aspects of the graph which have been semantically tagged more than the average number of times as interests.
These selected items are the overlay on the graph which represents the user model.

One potential thing that can be done is aggregating sections of the graph overlay, so we can include information in the user model at a concept level rather than just scattered across the graph.
This is the process of building a \textbf{concept-based user model}, and builds upon graph-based processing steps.

If for example the graph was a tree structure, grouping by concept allows us to select the nodes further up the tree which are considered to be interests, which in turn allows us to traverse further down into other branches of the tree which could also be relevant.

\subsection*{Using Stereotypes}

A \textbf{cold start} occurs when the system has insufficient information to begin adaptation for the user.
This is a typical issue in user modelling and adaptation.

\textbf{Stereotypes} were originally introduced in 1979, and researched over the next 5--10 years before being put on hold.
With the modern availability of big data and the need to adapt to a much broader range of users, stereotypes have become more popular again.
The key idea is that rather than personalising for every individual, we instead personalise to the user group(s) that our user belongs to.

Stereotyping is a popular technique to build models of users very quickly by assigning some attributes as true unless it is proven otherwise.
This is done because it can take time to learn `enough' about a user, and assigning a stereotype is very quick.

\fbox{
    \begin{minipage}{0.96\linewidth}
        Stereotype is a knowledge structure that represents frequently occurring characteristics of users and enables systems to make a large number of plausible inferences on the basis of a substantially smaller number of observations. \textbf{(Rich, 1979)}
    \end{minipage}
}

\subsubsection*{Structure}

\textbf{Body}: This contains information that is typically true for users which the stereotype applies to.
This includes the \textbf{facets} (characteristics), \textbf{values} (for quantifying facets), \textbf{ratings} (the degree of certainty for facet-value pairs), and sometimes justifications.

\textbf{Trigger}: The occurrence of particular events which signal the appropriateness of certain stereotypes.
These can be assigned a probability value.

\textbf{Relations}: These are between this stereotype and others in the system, used for particularly complex models.

\subsubsection*{Calculating Stereotypes}

Below, $s$ is the stereotype the probability is being calculated for.

\vspace*{-0.33cm}

\[
    P_{s}(facet=value) \\ = P(facet=value \mid s) \times P(s)
\]

\vspace*{-0.2cm}

If a facet is covered in multiple stereotypes, the union rule must be used to correctly calculate the combined probability:

\vspace*{-0.33cm}

\[
    P(facet=value) = X+(1-X) \times Y
\]

where $X = P_{a}(facet=value)$ and $Y = P_{b}(facet=value)$.

\subsubsection{Calculating Stereotypes - Example}

\textbf{Professional:}

\texttt{Trigger: (age>20) and (age<60) and (job in ProfessionalJobs) -> P=0.8}

\begin{table}[ht!]
    \centering
    \begin{tabular}{|l|l|l|}
        \hline
        Facet & Value & Rating \\
        \hline
        distance & long & 0.5 \\
        \hline
        exotic-place & 8 out of 10 & 0.5 \\
        \hline
        hotel-category & 3* & 0.6 \\
        & 4* & 0.2 \\
        \hline
        price & medium & 0.8 \\
        \hline
        entertainment & museums & 0.7 \\
        & theatre & 0.6 \\
        & bowling & 0.3 \\
        & leisure-centre & 0.6 \\
        & night-clubs & 0.5 \\
        \hline
    \end{tabular}
    \label{tab:stereotype-professional}
\end{table}

\pagebreak

\textbf{Man:}

\texttt{Trigger: (gender=`man') -> P=1.0 \\ OR Trigger: (name->`boy name') -> P=0.7}

\begin{table}[ht!]
    \centering
    \begin{tabular}{|l|l|l|}
        \hline
        Facet & Value & Rating \\
        \hline
        distance & long & 0.8 \\
        \hline
        sport-activities & 8 out of 10 & 0.6 \\
        \hline
        facilities & rent-a-car & 0.8 \\
        & tv & 0.8 \\
        & bowling & 0.2 \\
        \hline
        entertainment & night-clubs & 0.3 \\
        \hline
    \end{tabular}
    \label{tab:stereotype-man}
\end{table}

Below is an example user to build a user model for:

\texttt{\textlangle name=Charlie, age=27, single, IT Consultant\textrangle}

For example, to calculate $P(distance=long)$ for Charlie we'd need to apply the following steps:

\vspace*{-0.5cm}

\begin{gather*}
    P(man) = 0.7 \\
    P(distance=long \mid man) = 0.8 \\
    P(professional) = 0.8 \\
    P(distance=long \mid professional) = 0.5
\end{gather*}

With these values, we can then calculate $P(distance=long)$ for each of the stereotypes:

\vspace*{-0.5cm}

\begin{align*}
    X &= P_{man}(distance=long) \\ &= P(distance=long \mid man) \times P(man) \\ &= 0.8 \times 0.7 \\ &= 0.56 \\\\
    Y &= P_{pro}(distance=long) \\ &= P(distance=long \mid pro) \times P(pro) \\ &= 0.5 \times 0.8 \\ &= 0.4 \\
\end{align*}

\vspace*{-0.5cm}

Finally, we use the union rule to combine these two values, giving us a final probability value for Charlie based on these stereotypes:

\vspace*{-0.5cm}

\begin{align*}
    P(distance=long) &= X+(1-X)\times Y \\ &= 0.56 + (1-0.56) \times 0.4 \\ &= 0.56 + 0.44 \times 0.4 \\ &= 0.736
\end{align*}

\subsubsection*{Building Stereotypes}

This can be done by asking experts -- humans who have experience with the specific user groups.
Several experts should be surveyed, and the answers aggregated, finally reaching a consensus.

Alternatively, stereotypes can be obtained from data instead.
This uses available measurements and metrics about users, deciding which attributes are to be used, and allows for meaningful clusters to be identified based on the selected attributes.
These clusters can then be analysed to identify attributes with significance, which are then used to form stereotypes.

When contradictions appear in stereotypes, if there is a general and specific stereotype which apply, then the specific one should be prioritised.
In other cases, it is necessary to determine the priority of the different stereotypes, and which information is necessary to determine applicability, and what sensible default values would be.

When tuning stereotypes, if evidence shows that predictions based on the stereotypes turned out to be correct, then these should be preserved in the future.
If instead a stereotype is not correct, it can be amended, whether that's the facet value/ratings, or the trigger values.

\subsubsection*{Summary}

Stereotypes are a quick and relatively simple method for making assumptions using limited observations.
They're applicable to a variety of domains, easy to implement, and allow cold starts to be dealt with.

There is a potential issue of reliability, since facet values and ratings are subjectively assigned -- it's crucial to tune well.
Also, they don't cater for individual differences, and there's an ad-hoc approach to handling contradictions.
They can also lead to bias if stereotypes are derived from under-representative data.
