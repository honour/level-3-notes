\section*{Responsible Personalisation}

\subsection*{Ethical Issues in User-Adaptive Systems}

\subsubsection*{Extreme \& Misleading Content}

Popularity boosts appearance of content, but isn't a good indicator of quality, accuracy or veracity.
This can result in inaccurate content being viewed more often.

Because algorithms are often designed to increase engagement, they will often present content which is increasingly extreme to prompt a reaction.
This is especially true when it comes to content which creates distrust of other media forms, because it encourages users to only seek information from one platform.

\subsubsection*{Social Trust}

User adaptive systems can undermine social trust.
Recommendations algorithms, especially in social media, can increase social division.
They will show users content which is likely to invite engagement and interaction.
This can result in bracketing users into groups, for example by political division, and then show emotional content which will affirm these beliefs.
It also allows for the connection with like-minded users and advertisers.

This negatively impacts on social trust because users are segregated by only interacting with people that have similar beliefs.
Also, once users have been categorised into these groups, it's likely that they will be fed content that exacerbates prejudices and demonises out-groups.
This advances a narrative that members of the other groups cannot be trusted.

Serendipity is finding something interesting or valuable without expecting it.
An openness to serendipity is a ``civic virtue'' based around recognising that one might be wrong and is interested to find the truth.
When a person limits the information they receive to things they will agree with and hears only from like-minded people, this risks ``fragmentation, polarization, and extremism''.

Common experiences are like a social glue, they promote social trust and allow people to solve collective problems.
User adaptive systems can reduce these experiences, by presenting different users with entirely different experiences and sets of information about the world.

This grouping can create ``echo chambers'' -- people always have their views confirmed and never get challenged on them.

\subsubsection*{Discrimination \& Bias}

This can include the use of data by advertisers to target users in a discriminatory way, and also algorithms amplifying some viewpoints over others where this might raise issues around fairness.

For example, it's been found that Twitter's algorithm would amplify right leaning politics, leading to concerns around fairness, and the possibility that platforms are essentially giving certain groups free advertising.

\subsubsection*{Solutions}

We should try to introduce serendipity and social cohesion using the algorithms we create.
Recommendations which direct people contrary to their usual behaviour can also be a positive thing.
It's also good to give users control over how recommendations work (for example allowing the filtering of `low-quality accounts').
This process can also be done automatically.
Adding friction to high-volume activities (e.g.\ liking posts) can help reduce algorithm manipulation and engagement bias.

\subsection*{Responsible Personalisation}

The ethical principles in the frameworks for responsible AI are rooting in fundamental rights, which must be respected to ensure systems are developed and deployed in a trustworthy manner.
Systems should respect human autonomy, ensuring we respect the freedom of human beings.
They should also prevent harm, both by avoiding causing it, and also exacerbating harm.
They should be fair, offering equal and just distribution, be free from unfair bias, and avoid deception.
They should be explicit, building trust through transparency and clear communication.

The documents provide a set of questions in different categories which should be checked off.

\textbf{Human Agency and Oversight}:
\begin{itemize}
    \itemsep-0.75em
    \item How does the system influence the user's decision-making?
    \item Can the system create confusion?
    \item Can the system influence the user's autonomy by creating over-reliance on the system or other users?
    \item Can the system influence the user's decisions in an undesirable way?
    \item Is there a risk of creating human attachment, stimulating addictive behaviour, or manipulating user behaviour?
\end{itemize}

\textbf{Technical Robustness}
\begin{itemize}
    \itemsep-0.75em
    \item Can the system have adversarial, critical or damaging effects (e.g.\ to human or societal safety) in case of risks or threats such as design or technical faults, defects, outages, attacks, misuse, inappropriate or malicious use?
    \item Can a low level of accuracy of the system result in critical, adversarial or damaging consequences?
    \item Are there measures to ensure that the data (including training data) used to develop the system is up-to-date, of high quality, complete and representative of the environment the system will be deployed in?
\end{itemize}

\textbf{Privacy \& Data Governance} \\
What is the impact of the system on:
\begin{itemize}
    \itemsep-0.75em
    \item The right to privacy
    \item The right to physical, mental, and moral integrity
    \item The right to data protection
\end{itemize}

\textbf{Transparency}
\begin{itemize}
    \itemsep-0.75em
    \item Traceability
    \item Explainability
    \item Open communication about the limitations of the system
\end{itemize}

\textbf{Diversity, Non-discrimination, and Fairness}
\begin{itemize}
    \itemsep-0.75em
    \item Inadvertent historic bias, incompleteness of data
    \item Unintended (in)direct prejudice/discrimination against certain groups, exacerbating prejudice and marginalisation
    \item Intentional exploitation of (consumer) biases
    \item User-centric, regardless of age, gender, abilities, etc.
    \item Accessible for people with disabilities
\end{itemize}

\textbf{Societal and Environmental Well-being}
\begin{itemize}
    \itemsep-0.75em
    \item Are there potential negative impacts of the system on the environment?
    \item Does the system impact human work and work arrangements (e.g. skills development)?
    \item Can the system have a negative impact on society at large or democracy?
\end{itemize}

\textbf{Accountability} \\
Mechanisms should be put in place to ensure responsibility for the development, deployment, and user of personalised systems.
\begin{itemize}
    \itemsep-0.75em
    \item Is an appropriate traceability process in place?
    \item Can the system be audited by independent 3rd parties?
    \item Are all risks documented and actioned upon?
\end{itemize}
