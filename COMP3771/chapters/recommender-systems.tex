\section*{Recommender Systems}

The motivation for recommender systems is there being too much content and too many choices to make.
These systems can help suggest content to users.

\subsection*{Structure}

Recommender algorithms will contain \textbf{background data} -- information that the system already had before information processing starts; \textbf{input data} -- information that the user must communicate to the system in order for it to generate recommendations; and the \textbf{algorithm} -- this combines background and input data to generate suggestions.

\subsection*{Content-based Recommenders}

These use features of the items in the set $I$ (background data), and the user's ratings of items within $I$ (input data), and a classifier is generated which takes the rating behaviour of a user and applies it to single items to determine if they should be recommended.

Recommendations are based on correlation between content and user preferences.
Classification of the content is based on facts (pre-defined) or key term extraction.

When the user interacts with the interface, we access their user model, which has been obtained as described previously.
The description of the content must align to how the user model is represented.
We combine the user model and content, and we performed filtering to recommend the content that is most relevant to the user.

\subsubsection*{User Model based on Keywords}

If a keyword based model is being used, it would be represented as a vector of keywords/terms that are of interest.
We use the profile to find out the weights for each of the keywords.
The content would have a vector representing the weight of each of these same terms.
These weights would likely have been calculated using TFIDF\@.
We use a formula to calculate the similarity of users and items, and take the top $k$ items as recommendations to provide:

\vspace*{-0.33cm}

\[
    sim(U, I) = \frac{U \bullet I}{\lvert U \lvert \times \lvert I \lvert} = \frac{\sum_{i=1}^{n} u_i \times w_i }{\sqrt{\sum_{i=1}^{n}u_{i}^{2} \times \sum_{i-1}^{n}w_{i}^{2}}}
\]

\vspace*{-0.2cm}

\subsubsection*{User Model based on Facets, Values, Ratings}

Here the user model is made of (facet, value) tuples related to the content ($\langle f_{1}v_{1} \rangle, \langle f_{2}v_{2} \rangle, \ldots, \langle f_{k}v_{k} \rangle$).
The user model is obtained from stereotypes, which provide facets with values and ratings:

\vspace*{-0.33cm}

\[
    U=(\langle f_{1}v_{1}r_{1} \rangle, \langle f_{2}v_{2}r_{2} \rangle, \ldots, \langle f_{n}v_{n}r_{n} \rangle)
\]

\vspace*{-0.2cm}

Items are represented in a similar fashion, but with an additional value which represents whether specific values hold for those facets:

\vspace*{-0.33cm}

\[
    I=(\langle f_{1}v_{1}i_{1} \rangle, \langle f_{2}v_{2}i_{2} \rangle, \ldots, \langle f_{k}v_{k}i_{k} \rangle) \text{, where } i_j \in \{0,1\}
\]

We then calculate the relevance of items to the user, order by this value, and then take the top $k$ items as recommendations:

\vspace*{-0.33cm}

\[
    rel(U, I) = \sum_{i=1}^{n}r_i \times i_j
\]

\subsubsection*{User Model based on Graph/Concepts}

We initially have a graph representing the domain model, with concepts and links related to the content.
The user model is an overlay on this graph, which identifies the concepts related to them (e.g.\ knowledge/interests):

\vspace*{-0.33cm}

\[
    U=(u_1,u_2,\ldots,u_n)
\]

\vspace*{-0.2cm}

The item model is represented in the same way, but instead represents the concepts this item relates to:

\vspace*{-0.33cm}

\[
    I=(c_1,c_2,\ldots,c_n)
\]

\vspace*{-0.2cm}

We calculate relevance in the same manner as shown above, selecting the top $k$ items to recommend.

\subsubsection*{Pros and Cons of Content-based Recommenders}

They focus on just the user (and are not influenced by other users of the system), they are fairly easy to implement, and it is easy to explain to the user why recommendations have been made.
They're also applicable in a variety of contexts.

Some limitations are that they suffer from the cold start problem, can be over-specialised (focussing on the user's choices), the reliability of user models will significantly affect recommendations, and it requires a description of all content being recommended, which can be limited by availability and reliability.

Content-based systems can be made more efficient in a number of ways.
One of these is by adding novelty/surprise items -- this prevents content all being incredibly similar and users being stuck in a `filter bubble'.
It can also help to expand user's interests over time.
Another method is through proximity -- the more easily the system allows the user to reach recommended content, the more efficient their use of the system will be.
Finally, context relevance is important for improving use of the system -- depending on what the user is currently doing/the environment they are in etc., content may be more or less relevant, and adapting for this can help improve functionality.

\subsection*{Knowledge-based Recommenders}

This will include features of the items in $I$, and knowledge about how these items could meet a user's needs (background data).
The input data is a description of the user's needs/interests.
We can then infer a match between an item $i$ and the needs of $u$.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{assets/knowledge-graph}
    \label{fig:knowledge-graph}
\end{figure}

Knowledge-based systems use a model of the world (like the graph shown above), and the user profile is based on concepts of the graph.
Content is then mapped to this knowledge model.

Knowledge-based recommender systems build on content-based filtering, and one of the main advantages are that the reasoning enables better targeting and explanation of content recommendation, and can offer a diversified experience.
The main limitations are similar to CBR -- cold starts and issues with content preparation.

\subsection*{Collaborative (Social) Recommenders}

These recommenders take ratings by the user group, $U$, of items in the set $I$ as background data, and the ratings by the specific user, $u$, of items in $I$ as input data.
The algorithm then attempts to identify users in $U$ which are similar to $u$, and extrapolates from their ratings of $i$ to decide which items to recommend.
The general idea is that ``if people like you prefer this content, then you might like it too''.

Other users' preferences can be collected using implicit or explicit methods depending on the application.
For example things like ratings on Netflix and Amazon allow for explicit collection, however we can also perform observations such as what users view, buy, or pages they visit as an implicit method.
This however, is less reliable.

\subsubsection*{User-User Collaborative Filtering}

This recommends items based on what similar users have liked.
We find the nearest neighbours (using $k$-nearest neighbour).

We can calculate the similarity between the current user and all other users with cosine similarity, excluding the target item from the calculation.
We do this for all users, with a higher value representing a higher similarity.
We now define the size of our neighbourhood, $k$, sort by similarity value, and then take the top $k$ items -- this is our neighbourhood.
To make the actual recommendations, we combine the numbers from neighbours and work out what should be recommended.
This is done using a weighted sum, which scans the neighbourhood and calculates the frequency for each item -- this can be combined with the rating value.

User-user collaborative filtering has a number of advantages, including the fact it taps into social interaction data, and that it can recommend items that are not linked to the user's earlier choices.
It considers the opinions of a wide spectrum of users, and is especially popular in social media applications, but can be applied to a broader set of situations.

It has some limitations, including data sparsity -- a large number of items but a small user base.
Individual characteristics are also not catered for, and they tend to recommend popular items (causing a convergence around a few items).
Privacy and trust also become important issues with these.

\subsubsection*{Item-Item Collaborative Filtering}

This concept is the same as user-user collaborative filtering, but compares similarity between items instead of between users.
Similarity is determined using the cosine rule, as above.
A problem that applies to both user-user and item-item is that there will be millions of items in some cases, but only a few of them will have values other than 0.
This means that the algorithm takes a very long time, even for a minimal result.
The most expensive calculation is that of similarity.

\subsubsection{Amazon's Scalability Solution}

Originally item wanted to use item-item filtering, and marking items as a 1 or 0 depending on whether the user has bought that item.
Their solution to calculating similarity for all items was to instead only do this by finding the customers who have purchased items that the user has, and calculating similarity for these.

\subsection*{Demographic Recommenders}

These have demographic information about the users of the system, and their ratings of items in the set as background data, and then the demographic information about a specific user as input data.
Users who are demographically similar are then identified, and their ratings are extrapolated to determine what to recommend to users.

\subsection*{Utility-based Recommenders}

These use features of items in the set $I$ as background data, and a utility function over items in $I$ which describes $u$'s preferences as the input data.
This function is applied to items in order to establish their ranking for the user.

\subsection*{Hybrid Recommenders}

Hybrid recommenders combine recommender algorithms, to build on their advantages, and overcome limitations.
Consider a user $U$ and item $I$, each different algorithm will produce a recommendation value for that item which indicates whether it should be shown to the user.
We need to come up with a way to combine these values in an effective manner.

\subsubsection{Weighted}

Here, the scores for several techniques are combined to produce a single recommendation value:

\vspace*{-0.33cm}

\[
    R = \frac{R_1 \times W_1 + R_2 \times W_2 + \ldots + R_n \times W_n}{W_1 + W_2 + \ldots + W_n}
\]

\vspace*{-0.2cm}

\subsubsection{Switching}

Here, the system will switch depending on recommendation techniques based on the situation.
The recommendation value it outputs will be the value produced by the most appropriate algorithm for the current situation.

\subsubsection{Mixed}

Mixed recommender systems combine the outputs from multiple different recommendation techniques and present them at the same time.

\subsubsection{Feature Combination}

Here, features from different recommendation data sources are combined into a single recommender algorithm.

\subsubsection{Cascade}

Here, one recommender refines the recommendations which are then provided by another.

\subsubsection{Feature Augmentation}

Here, the output from one technique is used as an input feature to another.

\subsubsection{Meta-Level}

Meta-Level recommenders use the model learned from one recommender as input to another recommender.

\subsection*{Evaluation}

The evaluation of recommender algorithms drives improvement.
If we are evaluating a single algorithm, we can discuss which parameter setting is better, and which cases the algorithm performs best/worst in.
If we have several algorithms, we can discuss which algorithm performs best, and how to select and hybridise.
For the recommender system as a whole, we can discuss the benefits and drawbacks of recommendations on user experience and business.

\subsubsection{Rating Accuracy}

In order to calculate accuracy we can compare the output of the recommender system with the actual value given by a user.
This is typically done by calculating \textbf{mean absolute error} or \textbf{root-mean-square error}:

\vspace*{-0.33cm}

\[
    MAE = \frac{1}{n} \sum_{i=1}^{n} \lvert a_i - p_i \rvert \\
\]

\vspace*{-0.2cm}

\[
    RMSE = \sqrt{\frac{1}{n} \sum_{i=1}^{n} (a_i - p_i)^2}
\]

\vspace*{-0.2cm}

Correlation coefficients can also be used -- the Pearson coefficient for parametric data, and the Spearman or Kendal coefficients for non-parametric data.

We can also predict usage using a confusion matrix.
Here we can map predicted and actual results, by marking whether an item was recommended, and whether it would have been useful to the user.
This is show below:

\begin{table}[ht!]
    \centering
    \begin{tabular}{|r|l|l|}
        \hline
        & \textbf{Recommended} & \textbf{Not Recommended} \\
        \hline
        \textbf{Used} & True Positives & False Negatives \\
        \hline
        \textbf{Not Used} & False Positives & True Negatives \\
        \hline
    \end{tabular}
    \label{tab:confusion-matrix}
\end{table}

\vspace*{-0.33cm}

\[
    \text{Precision} = \frac{TP}{TP+FP}
\]

\vspace*{-0.2cm}

\[
    \text{Recall (True Positive Rate)} = \frac{TP}{TP+FN}
\]

\vspace*{-0.2cm}

\[
    \text{Fall-out (False Positive Rate)} = \frac{FP}{FP+TN}
\]

\vspace*{-0.2cm}

\subsubsection{Coverage}

Coverage can also be used to evaluate the effectiveness of recommender systems.
A system may be more accurate than another, but if it achieves this by only recommending a subset of the items (ones that would be considered easy to recommend), then this isn't ideal.
We can refer to the long tail -- the large number of unique items which may only have a few ratings or interactions.
Systems that can handle this long tail well, and recommend effectively across the entire inventory of items can be very useful.

\subsubsection{Novelty/Serendipity}

It's also valuable to recommend items that are new to the user, but they may still enjoy.
This can be done through \textbf{novelty} -- recommending items not in the prediction, but close to it, and \textbf{serendipity} -- items which are new but still relevant.
Collaborative filtering have novelty and serendipity embedded in the algorithms, but they happen by chance though the social factor ensures content relevance.
Content- and knowledge-based recommenders can provide more sophisticated ways to ensure novelty/serendipity, with the graph structure allowing reasoning.

\subsubsection{Diversity}

We can assess how diverse the list of items that we recommend is.
There are a number of different metrics for this: \textbf{variety} -- are we including content from all categories?; \textbf{balance} -- are the items from the categories proportional to the number of items in those categories?; \textbf{disparity} -- how far apart are the items, noting relevance with clustering.
