\section*{Injection Attacks}

When writing applications, some parameters used in application code can leave weaknesses.
This sometimes allows for exploits via user input fields.

Input validation is caused by parameters not being verified/checked by the system.
They are trusted to contain safe values, however we should trust nothing, and instead always verify.

\subsection*{Command Injection}

Untrusted data is combined with trusted data, typically in the form of strings.
This resulting string is passed to a compiler/interpreter.
Malicious inputs can be crafted to execute arbitrary code on the server.

This type of attack often be detected by identifying simple string concatenation, use of shell or subprocess commands (e.g.\ \texttt{exec()}) or code running with elevated privileges.

Command injection can be prevented by never using user input in dynamically evaluated code, and if this cannot be avoided then strict input validation must occur.

\subsection*{SQL Injection}

Same principles as command injection, but the target is instead SQL queries.
The impact of SQL injection will depend on where the vulnerability is in the code, the level of access required, and the ease of exploitation.

SQL injection should NOT be fixed with manual input validation, but instead \textbf{prepared statements} should be used.

\subsection*{Input Validation}

These types of attack succeed because too much trust is put into user input.
\textbf{All user input is malicious unless proven otherwise}.

\textbf{Homograph Attacks}: Domains can be registered using different scripts (e.g.\ Cyrillic) to trick using into visiting malicious websites.

\textbf{URL Obfuscation}: URLs can contain a user part and location part.
Historically, if the end of the user part contained a null-byte, some browsers would only show the user part, allowing users to be tricked.
\\ e.g.\ \texttt{http://www.trusted.com\%00@www.evil.com}

\textbf{Directory Traversal}: URLs can be crafted contained dots and slashes, to traverse through the filesystem on a server.

\subsection*{Canonicalisation}

Canonicalisation is the process of converting the different possible representations of a name/identifier into a single, standard, version.
Applications can often make incorrect choices based on non-canonical versions of names (e.g.\ allowing a username to be registered which looks identical to an already registered user), so the solution is to always canonicalise before making security decisions.

\subsection*{Buffer Overruns}

A program may allocate a block of memory of fixed size to store some data.
If the data stored in this buffer exceeds its capacity, a buffer overrun occurs.
The memory that comes after the buffer is overwritten, with possibly dangerous consequences.

These typically occur in low-level languages, like C/C++, where memory operations are not bounds-checked by default.
There can be serious consequences, as critical applications are often written in these languages.

One common way this is achieved is by `Stack Smashing'.
This involves overrunning the buffer, and modifying the return address stored on the stack.
When the current function finishes executing, the program will jump to attacker-controlled code.

In the worst-case scenario, the application is running with elevated privileges, and the attacker modifies the buffer to contain malicious code, and then changes the return address to point to this input.

A typical exploit may have the following construction: \\
\includegraphics[width=\linewidth]{assets/buffer}

\texttt{libc} provides an \texttt{exec(arg)} function, which takes a string pointer, so an exploit simply needs to parameterise and call this function correctly.
Generally, modification of the control flow is required to jump to the attacker's code.

This can be achieved by overflowing a buffer with few bounds checks, to corrupt adjacent pointers.
Because the attacker can insert bytes of their choice, C's type system and the program's logic can be bypassed.
There are a few options as to which state the attacker can corrupt.

One option is \textbf{activation records}.
These records are created on the stack each function call, and include the address to return to after.
Corrupting this return address allows jumping to attacker code, as explained above.

The other option is \textbf{function pointers}.
The goal here is to overwrite the buffer and modify a function pointer.
Then, when the legitimate code attempts to call this function, it will instead call the attacker's code.

A buffer overrun attack is still possible with very small stack frames, if the attacker can inject shellcode into another area.
Buffer overruns are still possible with overruns as small as one byte (an off-by-one error).

\subsection*{Heap Overruns}

This can be used to overwrite application variables, as with the stack.
It can also be used to overwrite metadata for managing storage.
This could allow a careful attacker to write to memory locations of their choice.

\subsection*{Defences}

Correct programming (e.g.\ manual bounds checking), safer library versions, compile time and runtime checking code and support from OS/hardware.

\textbf{Correct programming} is particularly difficult in C, but static analysis, and searching for vulnerable calls is a good start.
Even defensive code can still suffer from buffer overruns.

\textbf{Non-executable buffers}: Make the data segment of the program's address space non-executable.
This makes it impossible for attackers to execute code injected into a buffer.
The whole data segment can't be made non-executable for performance reasons, but parts of it can be (e.g.\ stack).
It's very rare for legitimate programs to have code in the data segment.

\textbf{Array bounds checking}: Control flow must be corrupted for a buffer overflow to succeed.
Array bounds checking prevents buffer overflow in the first place.
All array reads/writes should be checked, but optimisations may result in fewer calls being checked.

\textbf{Code pointer integrity}: This doesn't prevent corruption like bounds checking.
Instead, it detects that a code pointer has been modified before it's dereferenced.
Therefore, an attacker may succeed in modifying, but it will never be used.
This doesn't however prevent against all buffer overflow issues.

\textbf{Stack Canary}: This is a value placed below the return address.
If this value has been modified, we can safely assume that the return address is no longer safe to use.
GCC offers a number of command-line arguments to enable these protections.

The operating system offers some protections, such as Address Space Layout Randomisation (ASLR), Pointer Authentication Codes (on Apple Silicon) and a non-executable stack and heap.
