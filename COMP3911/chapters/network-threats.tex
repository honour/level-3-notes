\section*{Network Threats}

\subsection*{DNS, ARP \& Application Protocols}

2 levels of address resolution, DNS and ARP\@.

\textbf{DNS:} Domain name $\rightarrow$ IP Address \\
\textbf{ARP:} IP Address $\rightarrow$ MAC Address

Spoofing is possible at both levels.
DNS caches replies to save time in future.
Most Unix DNS implementations use BIND, subject to buffer overflows.

DNS can be attacked in 2 main ways.

\textbf{Cache Poisoning:} Spurious cache entries \\
\textbf{Registrar Hijack:} Allows full control of records

\subsubsection*{DNS Cache Poisoning}

This can be achieved by racing to respond to a query with a malicious reply.
A transaction ID (16-bit) value must be correct for the reply to be trusted.

If this is incorrect the true reply gets cached and delays further attacks.

But attackers can send thousands of attacks with different TX IDs, and attempt to poison related subdomains.

\subsubsection*{ARP Cache Poisoning}

Similar to with DNS - hijack the response to a query and return an attack-controlled MAC address.

\includegraphics[width=\linewidth]{assets/arp}

\section*{Remote Access}

Historically, remote logins used \texttt{telnet} or \texttt{rlogin}.
These are \textbf{unencrypted}, as are \texttt{rsh}, \texttt{rcp}, etc.
This allows for spoofing to be used to capture credentials!
(e.g.\ ARP spoofing can be used to view traffic intended for a different machine)

\subsection*{Banner Grabbing}

Some protocols will provide a `banner' when connecting.
Banner Grabbing is the process of connecting on these ports with a tool like \texttt{telnet}, to view the banner.

e.g.\ SSH will respond with its version -- this allows attackers to search for vulnerabilities which apply to this version.

\subsection*{Secure Tools}

The `\texttt{r}'-tools have since been replaced by `\texttt{s}'-versions.
These are cryptographically-enabled versions which use symmetric encryption.
PKC is used for client auth and symmetric key exchange.

This is done either with a password or keypair of which the public key is placed on the remote machine.

SSH is a complex, custom protocol.
SSH-1 is now considered insecure and was replaced by SSH-2, but this has also had issues.

\subsubsection*{Man-in-the-middle Attacks}

If an attacker impersonates a server they could steal credentials.

SSH mitigates this using a `hosts' file - it locally stores a list of trusted servers, and can warn if the host key changes.

\subsection*{Email}

\textbf{SMTP} is a very simple request-response protocol on port 25.
It lacks authentication and sends all traffic in plaintext.
Subject to banner grabbing.
Open relays are heavily abused for spam.

\textbf{POP3} and \textbf{IMAP} are used for receiving mail.
They support weak auth options and mail is unprotected.

\subsubsection*{Securing Mail}

SSH offers a solution by tunneling an insecure protocol.
But mail protocols now all support TLS\@.
Very similar to HTTP(S) - use a different port for secure traffic.

Standard ports also support `STARTTLS' - allows an upgrade to TLS communication.

\subsubsection*{Tradeoffs}

Ideally mail would be E2EE, but email is a common malware vector.
Gateway scanning can't be performed on encrypted mail.

\section*{Malware}

\textbf{Require Host:} Backdoor, Logic Bomb, Virus \\
\textbf{Independent:} Trojan Horse, Worm

\textbf{Supply chain attacks} - attacking third-party libraries that many people rely on.
e.g.\ Typosquatting, compromising update processes

\subsection*{Viruses}

Many different types, e.g.\ memory-resident, parasitic, boot record.
Can lie dormant until triggered.
Propagation and execution of payload are two distinct stages.

\includegraphics[width=\linewidth]{assets/virus}

\textbf{Polymorphism} is used to evade signature detection.
It can involve NOP insertion, instruction swapping, etc.\ to create identical functionality but different executables.

Virus detection has gone through multiple stages:
Signature detection, compression/encryption detection and integrity checking with HMACs, action over signature detection, sandboxing.

\subsection*{Trojan Horses}

Pose as legitimate programs but have covert behaviour.
RATs start a server allowing for complete control by an attacker.

Typical vectors include tempting websites, email attachments, phishing.

\subsection*{Worms}

Typically propagate through transport (TCP/UDP) or application later (Email/HTTP).
Usually they will identify targets, establish a connection, copy the worm, and execute it.

\subsubsection*{Modern Worm Trends}

Broader range of attack vectors, reliance on email and social media.
Ransomware functionality and botnet recruitment are common.
Usage by state actors for cyberwarfare is also happening.

\subsubsection*{Ransomware}

Malware includes a public key, and infects victim via trojan or worm.
Symmetric cipher key is used to quickly encrypt files.
Cipher key is then encrypted by the public key - only the attacker can then decrypt files when payment is provided.
