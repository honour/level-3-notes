# Level 3 Notes - University of Leeds, Computer Science

These are my notes for the modules I took during my final year at University.
I've tried to make them as comprehensive as possible, but it's likely that certain things haven't been explained in enough detail, or assume prior knowledge.
Hopefully they can be of some use to people taking any of the modules regardless.

## Compiled Versions

The compiled documents are automatically generated and uploaded to an S3 bucket.
They can be accessed using the following URL pattern:

```
https://documents.xela.tech/{module}.pdf
```

For example, https://documents.xela.tech/COMP3911.pdf returns the most up-to-date version of the Secure Computing (COMP3911) notes.

## Modules

| Module Code | Module Title                      | Link                                                       |
|-------------|-----------------------------------|------------------------------------------------------------|
| COMP3011    | Web Services and Web Data         | [COMP3011.pdf](https://documents.xela.tech/COMP3011.pdf)   |
| COMP3211    | Distributed Systems               | [COMP3211.pdf](https://documents.xela.tech/COMP3211.pdf)   |
| COMP3771    | User Adaptive Intelligent Systems | [COMP3771.pdf](https://documents.xela.tech/COMP3771.pdf)   |
| COMP3911    | Secure Computing                  | [COMP3911.pdf](https://documents.xela.tech/COMP3911.pdf)   |
| COMP5123M   | Cloud Computing Systems           | [COMP5123M.pdf](https://documents.xela.tech/COMP5123M.pdf) |
